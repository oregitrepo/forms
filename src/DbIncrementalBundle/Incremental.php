<?php
/**
 * Created by PhpStorm.
 * User: rczapski
 * Date: 2015-08-30
 * Time: 09:39
 */

namespace DbIncrementalBundle;

use DbIncrementalBundle\IncrementalModel;

class Incremental
{
    const PROCEDURES_PATH = "Incrementals/";
    protected $path;
    protected $incrementalsList = null;
    protected $dbConnection;
    protected $mode;

    /**
     * DbProcedure constructor.
     *
     * @param $incrementalsList
     */
    public function __construct(\PDO $dbConnection, $mode = 'regular')
    {
        $this->setPath();
        $this->setIncrementalsList();
        $this->dbConnection = $dbConnection;
        $this->setIncrementalModel($this->dbConnection);
        $this->mode = $mode;
    }

    protected function setIncrementalModel($dbConnection)
    {
        $this->incrementalModel = new IncrementalModel($dbConnection);
    }

    protected function setPath()
    {
        $path =  realpath(dirname(__FILE__));
        $this->path = $path.'/'.static::PROCEDURES_PATH;
    }

    protected function setIncrementalsList()
    {
        foreach (new \DirectoryIterator($this->path) as $fileInfo) {
            if($fileInfo->isDot()) continue;

            if ('sql' == $fileInfo->getExtension()) {
                $this->incrementalsList[] = $fileInfo->getBasename('.sql');
            }
        }

        sort($this->incrementalsList);
    }

    protected function checkIfErrorOccurred()
    {
        $error = $this->dbConnection->errorInfo();

        if (null !== $error[2]) {
            $this->sqlOccurredError = $error[2];
            var_dump($this->sqlOccurredError);
            exit;
        }
    }

    public function clearDb()
    {
        $this->incrementalModel->clear();
    }

    public function reBuildIncremental()
    {
        $toUpdateParameter = isset($_GET['update'])?$_GET['update']:null;

        if (null != $this->incrementalsList) {

            foreach($this->incrementalsList as $incrementalName) {

                $crc32FromFileContent = crc32(file_get_contents($this->path.$incrementalName.'.sql'));
                $SqlFileContent = file_get_contents($this->path . $incrementalName.'.sql');

                if ($this->incrementalModel->isToRebuild($incrementalName)) {
                    $this->incrementalModel->rebuild($incrementalName,crc32($SqlFileContent));
                }
                elseif ($this->incrementalModel->isChanged($incrementalName, $crc32FromFileContent)) {

                    if ('regular' == $this->mode) {
                        echo "\e[36m Update: $incrementalName \e[0m\n";
                    }

                    if ($toUpdateParameter !== null) {

                        $this->dbConnection->exec($SqlFileContent);
                        $this->checkIfErrorOccurred();

                        $this->incrementalModel->update($incrementalName,crc32($SqlFileContent));
                    }
                }
                elseif($this->incrementalModel->isExist($incrementalName)) {

                    if ('regular' == $this->mode) {
                        echo "\e[0m Present: $incrementalName \e[0m\n";
                    }
                }
                else {

                    if ('regular' == $this->mode) {
                        echo "\e[32m New: $incrementalName \e[0m\n";
                    }

                    $this->dbConnection->exec($SqlFileContent);
                    $this->checkIfErrorOccurred();

                    $this->incrementalModel->add($incrementalName,crc32($SqlFileContent));
                }
            }
        }
    }
}