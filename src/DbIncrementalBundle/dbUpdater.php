<?php

$mode = php_sapi_name();

$loader = require '../../app/autoload.php';


$host = 'localhost';
$dbname = 'dbname';
$user = 'root';
$password = 'root';

$dbConnection = $dbh = new PDO("mysql:host=$host;dbname=$dbname", "$user", "$password",[PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING]);
$incrementals = new DbIncrementalBundle\Incremental($dbConnection);

if(isset($argv[1])) {
    $command = $argv[1];
} else {
    $command = isset($_GET['command'])?$_GET['command']:null;
}

if (null === $command) {
    echo "Podaj parametr command [regular, clearDb]";
}

if ($command == 'clearDb') {
    echo "Tabela incremetali została wyczyszczona";
    $incrementals->clearDb();
    die;
}

if ($command == 'regular') {
    echo "\nBaza $dbname\n\n";

    $incrementals->reBuildIncremental();
}
?>





