<?php

namespace DbIncrementalBundle;


class IncrementalModel
{
    protected $dbConnection;
    /**
     * DbConnector constructor.
     */
    public function __construct($dbConnection)
    {
        $this->dbConnection = $dbConnection;
        $this->createIncrementalsTable();
    }

    public function clear()
    {
        $query = $this->dbConnection->prepare("DELETE FROM incrementals");
        $query->execute();
    }

    // TODO fix to bindValues
    public function isExist($fileName)
    {
        $result = false;

        $query = $this->dbConnection->prepare("SELECT name FROM incrementals WHERE name = :file_name");
        $query->bindValue(":file_name", $fileName, \PDO::PARAM_STR);
        $query->execute();
        $row = $query->fetchAll(\PDO::FETCH_ASSOC);

        if ($row) {
            $result = true;
        }

        return $result;
    }

    // TODO fix to bindValues
    public function isToRebuild($fileName)
    {
        $result = false;

        $query = $this->dbConnection->prepare("SELECT name FROM incrementals WHERE name = :file_name AND isToRebuild = :isRebulid");
        $query->bindValue(":file_name", $fileName, \PDO::PARAM_STR);
        $query->bindValue(":isRebulid", true);
        $query->execute();
        $row = $query->fetchAll(\PDO::FETCH_ASSOC);

        if ($row) {
            $result = true;
        }

        return $result;
    }

    // TODO fix to bindValues
    public function isChanged($fileName,$checkSum)
    {
        $result = false;

        $query = $this->dbConnection->prepare("SELECT name FROM incrementals WHERE name = :file_name and check_sum != :checkSum");
        $query->bindValue(":file_name", $fileName, \PDO::PARAM_STR);
        $query->bindValue(":checkSum", $checkSum, \PDO::PARAM_STR);
        $query->execute();
        $row = $query->fetchAll(\PDO::FETCH_ASSOC);

        if ($row) {
            $result = true;
        }

        return $result;
    }

    public function add($fileName,$checkSum)
    {
        $sql = "INSERT INTO incrementals (name, check_sum, create_date) VALUES (:file_name, :checkSum, :createDate)";
        $query = $this->dbConnection->prepare($sql);

        $query->bindValue(":file_name", $fileName, \PDO::PARAM_STR);
        $query->bindValue(":checkSum", $checkSum, \PDO::PARAM_STR);
        $query->bindValue(':createDate', date("Y-m-d H:i:s"), \PDO::PARAM_STR);

        $query->execute();
    }

    public function update($fileName,$checkSum)
    {
        $sql = "UPDATE incrementals SET check_sum = :checkSum, modified_data = :modifiedData WHERE name = :file_name";
        $query = $this->dbConnection->prepare($sql);

        $query->bindValue(":file_name", $fileName, \PDO::PARAM_STR);
        $query->bindValue(":checkSum", $checkSum, \PDO::PARAM_INT);
        $query->bindValue(":modifiedData", date('Y-m-d H:i:s'));
        $query->execute();
    }

    public function rebuild($fileName,$checkSum)
    {
        $sql = "UPDATE incrementals SET check_sum = :checkSum, modified_data = :modifiedData, isToRebuild = :isToRebuild WHERE name = :file_name";
        $query = $this->dbConnection->prepare($sql);

        $query->bindValue(":file_name", $fileName, \PDO::PARAM_STR);
        $query->bindValue(":checkSum", $checkSum, \PDO::PARAM_INT);
        $query->bindValue(":isToRebuild", false);
        $query->bindValue(":modifiedData", date('Y-m-d H:i:s'));
        $query->execute();
    }

    public function createIncrementalsTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `incrementals` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                name VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
                `check_sum` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
                `create_date` DATETIME NULL DEFAULT NULL,
                `modified_data` DATETIME NULL DEFAULT NULL,
                `isToRebuild` TINYINT(1) NOT NULL DEFAULT '0',
                PRIMARY KEY (`id`),
                UNIQUE INDEX name (name)
            )
            COLLATE='utf8mb4_unicode_ci'
            ENGINE=InnoDB";

        $query = $this->dbConnection->prepare($sql);
        $query->execute();
    }
}