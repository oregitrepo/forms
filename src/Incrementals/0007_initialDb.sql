DROP TABLE IF EXISTS public.forms_groups;
DROP TABLE IF EXISTS public.forms_groups_to_group;
DROP TABLE IF EXISTS public.forms_to_forms_groups;

CREATE TABLE public.forms_groups (
	id int4 NOT NULL,
	"name" varchar(255) NOT NULL,
	description text NOT NULL,
	CONSTRAINT forms_groups_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE public.forms_groups_to_group (
	id int4 NOT NULL,
	forms_group_id int4 NOT NULL,
	group_id int4 NOT NULL,
	CONSTRAINT forms_groups_to_group_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE public.forms_to_forms_groups (
	id int4 NOT NULL,
	forms_id int4 NOT NULL,
	forms_group_id int4 NOT NULL,
	CONSTRAINT forms_to_forms_groups_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;