INSERT INTO public.forms_groups
(id, "name", description)
VALUES(1, 'Narzędzia diagnostyczne', '');
INSERT INTO public.forms_groups
(id, "name", description)
VALUES(2, 'Ćwiczenia', '');
INSERT INTO public.forms_groups
(id, "name", description)
VALUES(3, 'Testowe', '');


INSERT INTO public.forms_groups_to_group
(id, forms_group_id, group_id)
VALUES(1, 1, 4);
INSERT INTO public.forms_groups_to_group
(id, forms_group_id, group_id)
VALUES(2, 1, 21);
INSERT INTO public.forms_groups_to_group
(id, forms_group_id, group_id)
VALUES(3, 2, 21);
INSERT INTO public.forms_groups_to_group
(id, forms_group_id, group_id)
VALUES(4, 3, 1);


INSERT INTO public.fos_group
(id, "name", roles)
VALUES(1, 'Admin', 'a:8:{i:0;s:10:"VIEW_FORMS";i:1;s:16:"VIEW_SUBMISSIONS";i:2;s:12:"VIEW_REPORTS";i:3;s:10:"VIEW_USERS";i:4;s:11:"VIEW_GROUPS";i:5;s:17:"VIEW_FORMS_GROUPS";i:6;s:14:"GENERATE_TOKEN";i:7;s:11:"SUBMIT_FORM";}');
INSERT INTO public.fos_group
(id, "name", roles)
VALUES(21, 'Pedagog', 'a:3:{i:0;s:10:"VIEW_FORMS";i:1;s:16:"VIEW_SUBMISSIONS";i:2;s:12:"VIEW_REPORTS";}');
INSERT INTO public.fos_group
(id, "name", roles)
VALUES(2, 'Redaktor', 'a:5:{i:0;s:10:"VIEW_FORMS";i:1;s:10:"VIEW_USERS";i:2;s:11:"VIEW_GROUPS";i:3;s:17:"VIEW_FORMS_GROUPS";i:4;s:11:"SUBMIT_FORM";}');
INSERT INTO public.fos_group
(id, "name", roles)
VALUES(3, 'Specjalista', 'a:2:{i:0;s:16:"VIEW_SUBMISSIONS";i:1;s:12:"VIEW_REPORTS";}');
INSERT INTO public.fos_group
(id, "name", roles)
VALUES(4, 'Psycholog', 'a:3:{i:0;s:10:"VIEW_FORMS";i:1;s:16:"VIEW_SUBMISSIONS";i:2;s:12:"VIEW_REPORTS";}');

