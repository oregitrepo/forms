DROP TABLE IF EXISTS public.forms;
DROP TABLE IF EXISTS public.users;
DROP TABLE IF EXISTS public.tokens;
DROP TABLE IF EXISTS public.groups;
DROP TABLE IF EXISTS public.submissions;
DROP TABLE IF EXISTS public.forms_types;
DROP TABLE IF EXISTS public.groups;


CREATE TABLE public.forms (
  id             SERIAL NOT NULL,
  form           JSON   NOT NULL,
  group_id       INT4   NULL,
  forms_types_id INT4   NULL,
  CONSTRAINT forms_pk PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);

CREATE TABLE public.submissions (
  id          SERIAL  NOT NULL,
  token       VARCHAR NOT NULL,
  form_id     INT4    NOT NULL,
  submission  JSON    NOT NULL,
  submit_date DATE    NOT NULL,
  CONSTRAINT submissions_pk PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);

CREATE TABLE public.tokens (
  token VARCHAR NOT NULL,
  CONSTRAINT tokens_pk PRIMARY KEY (token)
)
WITH (
OIDS = FALSE
);

CREATE TABLE public.groups (
  id     SERIAL  NOT NULL,
  "name" VARCHAR NOT NULL,
  roles  TEXT    NOT NULL,
  CONSTRAINT groups_pk PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);

CREATE TABLE public.forms_types (
  id     SERIAL  NOT NULL,
  "name" VARCHAR NOT NULL,
  CONSTRAINT forms_types_pk PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);

INSERT INTO forms (form) VALUES ('{
  "_id": "5912dbc64d22cb10edaeecdc",
  "machineName": "pierwszyFormularz",
  "modified": "2017-05-10T10:17:34.702Z",
  "display": "form",
  "title": "Pierwszy formularz",
  "name": "pierwszyFormularz",
  "path": "form1",
  "created": "2017-05-10T09:22:14.166Z",
  "components": [
    {
      "input": true,
      "tableView": true,
      "inputType": "text",
      "inputMask": "",
      "label": "Imię",
      "key": "imie",
      "placeholder": "Podaj swoje imię",
      "prefix": "",
      "suffix": "",
      "multiple": false,
      "defaultValue": "",
      "protected": false,
      "unique": false,
      "persistent": true,
      "clearOnHide": true,
      "validate": {
        "required": true,
        "minLength": 3,
        "maxLength": 50,
        "pattern": "([a-zA-Z])+",
        "custom": "",
        "customPrivate": false
      },
      "conditional": {
        "show": "",
        "when": null,
        "eq": ""
      },
      "type": "textfield",
      "tags": [],
      "disabled": false,
      "lockKey": true
    },
    {
      "input": true,
      "tableView": true,
      "inputType": "text",
      "inputMask": "",
      "label": "Nazwisko",
      "key": "nazwisko",
      "placeholder": "Podaj swoje nazwisko",
      "prefix": "",
      "suffix": "",
      "multiple": false,
      "defaultValue": "",
      "protected": false,
      "unique": true,
      "persistent": true,
      "clearOnHide": true,
      "validate": {
        "required": true,
        "minLength": 2,
        "maxLength": 60,
        "pattern": "([a-zA-Z])+",
        "custom": "",
        "customPrivate": false
      },
      "conditional": {
        "show": "",
        "when": null,
        "eq": ""
      },
      "type": "textfield",
      "tags": []
    },
    {
      "input": true,
      "tableView": true,
      "inputType": "number",
      "label": "Wiek",
      "key": "wiek",
      "placeholder": "Podaj swój wiek",
      "prefix": "",
      "suffix": "",
      "defaultValue": "",
      "protected": false,
      "persistent": true,
      "clearOnHide": true,
      "validate": {
        "required": true,
        "min": 0,
        "max": 200,
        "step": "any",
        "integer": "",
        "multiple": "",
        "custom": ""
      },
      "type": "number",
      "tags": [],
      "conditional": {
        "show": "",
        "when": null,
        "eq": ""
      }
    },
    {
      "input": true,
      "tableView": true,
      "label": "Opis",
      "key": "opis",
      "placeholder": "Napisz coś o sobie",
      "prefix": "",
      "suffix": "",
      "rows": 3,
      "multiple": false,
      "defaultValue": "",
      "protected": false,
      "persistent": true,
      "wysiwyg": false,
      "clearOnHide": true,
      "validate": {
        "required": false,
        "minLength": 0,
        "maxLength": 500,
        "pattern": "([A-z0-9.:+-@])+",
        "custom": ""
      },
      "type": "textarea",
      "tags": [],
      "conditional": {
        "show": "",
        "when": null,
        "eq": ""
      }
    },
    {
      "input": true,
      "tableView": true,
      "label": "Zawód",
      "key": "zawd",
      "placeholder": "Wybierz zawód",
      "data": {
        "values": [
          {
            "value": "tokarz",
            "label": "Tokarz"
          },
          {
            "value": "piekarz",
            "label": "Piekarz"
          },
          {
            "value": "lekarz",
            "label": "Lekarz"
          },
          {
            "value": "student",
            "label": "Student"
          }
        ],
        "json": "",
        "url": "",
        "resource": "",
        "custom": ""
      },
      "dataSrc": "values",
      "valueProperty": "",
      "defaultValue": "",
      "refreshOn": "",
      "filter": "",
      "authenticate": false,
      "template": "<span>{{ item.label }}</span>",
      "multiple": false,
      "protected": false,
      "unique": false,
      "persistent": true,
      "clearOnHide": true,
      "validate": {
        "required": true
      },
      "type": "select",
      "tags": [],
      "conditional": {
        "show": "",
        "when": null,
        "eq": ""
      },
      "isNew": false
    },
    {
      "input": true,
      "inputType": "checkbox",
      "tableView": true,
      "hideLabel": true,
      "label": "Bezrobotny",
      "datagridLabel": true,
      "key": "bezrobotny",
      "defaultValue": false,
      "protected": false,
      "persistent": true,
      "clearOnHide": true,
      "validate": {
        "required": false
      },
      "type": "checkbox",
      "tags": [],
      "conditional": {
        "show": "",
        "when": null,
        "eq": ""
      },
      "lockKey": true
    },
    {
      "input": true,
      "tableView": true,
      "inputType": "email",
      "label": "Email",
      "key": "email",
      "placeholder": "Podaj swój adres e-mail",
      "prefix": "",
      "suffix": "",
      "defaultValue": "",
      "protected": false,
      "unique": false,
      "persistent": true,
      "clearOnHide": true,
      "kickbox": {
        "enabled": false
      },
      "type": "email",
      "tags": [],
      "conditional": {
        "show": "",
        "when": null,
        "eq": ""
      },
      "validate": {
        "required": true,
        "minLength": 0,
        "maxLength": 200,
        "pattern": "[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+"
      }
    },
    {
      "input": true,
      "label": "Submit",
      "tableView": false,
      "key": "submit",
      "size": "md",
      "leftIcon": "",
      "rightIcon": "",
      "block": false,
      "action": "submit",
      "disableOnInvalid": false,
      "theme": "primary",
      "type": "button"
    }
  ],
  "owner": "591026c404309f0a5e15049f",
  "submissionAccess": [
    {
      "type": "create_own",
      "roles": [
        "5910268804309f0a5e150492"
      ]
    },
    {
      "type": "read_own",
      "roles": [
        "5910268804309f0a5e150492"
      ]
    },
    {
      "type": "update_own",
      "roles": [
        "5910268804309f0a5e150492"
      ]
    },
    {
      "type": "delete_own",
      "roles": [
        "5910268804309f0a5e150492"
      ]
    }
  ],
  "access": [
    {
      "type": "read_all",
      "roles": [
        "5910268804309f0a5e150491",
        "5910268804309f0a5e150492",
        "5910268804309f0a5e150493"
      ]
    }
  ],
  "tags": [
    "common"
  ],
  "type": "form"
}');
INSERT INTO forms (form) VALUES ('{
  "_id": "5912df014d22cb10edaeece0",
  "machineName": "drugiFormularz",
  "modified": "2017-05-10T10:17:58.257Z",
  "display": "form",
  "title": "Drugi formularz",
  "name": "drugiFormularz",
  "path": "form2",
  "created": "2017-05-10T09:36:01.524Z",
  "components": [
    {
      "validate": {
        "maxLength": 200,
        "minLength": 3,
        "pattern": "[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+",
        "required": true
      },
      "conditional": {
        "eq": "",
        "when": null,
        "show": ""
      },
      "tags": [],
      "type": "email",
      "kickbox": {
        "enabled": false
      },
      "clearOnHide": true,
      "persistent": true,
      "unique": true,
      "protected": false,
      "defaultValue": "",
      "suffix": "",
      "prefix": "",
      "placeholder": "Podaj email",
      "key": "email",
      "label": "Email",
      "inputType": "email",
      "tableView": true,
      "input": true
    },
    {
      "conditional": {
        "eq": "",
        "when": null,
        "show": ""
      },
      "tags": [],
      "type": "phoneNumber",
      "validate": {
        "required": true
      },
      "clearOnHide": true,
      "defaultValue": "",
      "persistent": true,
      "unique": false,
      "protected": false,
      "multiple": false,
      "suffix": "",
      "prefix": "+48",
      "placeholder": "",
      "key": "telefonkomrkowy",
      "label": "Telefon komórkowy",
      "inputMask": "999-999-999",
      "tableView": true,
      "input": true
    },
    {
      "conditional": {
        "eq": "",
        "when": null,
        "show": ""
      },
      "tags": [],
      "type": "address",
      "validate": {
        "required": true
      },
      "map": {
        "key": "",
        "region": ""
      },
      "persistent": true,
      "unique": false,
      "clearOnHide": true,
      "protected": false,
      "multiple": false,
      "placeholder": "Podaj adres",
      "key": "adres",
      "label": "Adres",
      "tableView": true,
      "input": true
    },
    {
      "conditional": {
        "eq": "",
        "when": null,
        "show": ""
      },
      "tags": [],
      "type": "day",
      "validate": {
        "custom": ""
      },
      "clearOnHide": true,
      "persistent": true,
      "protected": false,
      "dayFirst": true,
      "fields": {
        "year": {
          "hide": false,
          "required": true,
          "placeholder": "rok",
          "type": "text"
        },
        "month": {
          "required": true,
          "placeholder": "miesiąc",
          "type": "select"
        },
        "day": {
          "required": true,
          "placeholder": "dzień",
          "type": "text"
        }
      },
      "key": "dataurodzenia",
      "label": "Data urodzenia",
      "tableView": true,
      "input": true
    },
    {
      "conditional": {
        "eq": "",
        "when": null,
        "show": ""
      },
      "tags": [],
      "type": "radio",
      "validate": {
        "customPrivate": false,
        "custom": "",
        "required": true
      },
      "clearOnHide": true,
      "persistent": true,
      "protected": false,
      "defaultValue": "",
      "values": [
        {
          "label": "Naleśniki",
          "value": "nalesniki"
        },
        {
          "label": "Placki ziemiaczane",
          "value": "plackiZiemiaczane"
        },
        {
          "label": "Pierogi",
          "value": "pierogi"
        },
        {
          "label": "Pizza",
          "value": "pizza"
        }
      ],
      "key": "cowolisz",
      "label": "Co wolisz?",
      "inputType": "radio",
      "tableView": true,
      "input": true
    },
    {
      "type": "button",
      "theme": "primary",
      "disableOnInvalid": false,
      "action": "submit",
      "block": false,
      "rightIcon": "",
      "leftIcon": "",
      "size": "md",
      "key": "submit",
      "tableView": false,
      "label": "Submit",
      "input": true
    }
  ],
  "owner": "591026c404309f0a5e15049f",
  "submissionAccess": [
    {
      "type": "create_own",
      "roles": [
        "5910268804309f0a5e150492"
      ]
    },
    {
      "type": "read_own",
      "roles": [
        "5910268804309f0a5e150492"
      ]
    },
    {
      "type": "update_own",
      "roles": [
        "5910268804309f0a5e150492"
      ]
    },
    {
      "type": "delete_own",
      "roles": [
        "5910268804309f0a5e150492"
      ]
    }
  ],
  "access": [
    {
      "type": "read_all",
      "roles": [
        "5910268804309f0a5e150491",
        "5910268804309f0a5e150492",
        "5910268804309f0a5e150493"
      ]
    }
  ],
  "tags": [
    "common"
  ],
  "type": "form"
}');
INSERT INTO forms (form) VALUES ('{
  "_id": "5912e1b04d22cb10edaeece2",
  "machineName": "trzeciFormularz",
  "modified": "2017-05-10T09:58:19.512Z",
  "display": "form",
  "title": "Trzeci formularz",
  "name": "trzeciFormularz",
  "path": "form3",
  "created": "2017-05-10T09:47:28.569Z",
  "components": [
    {
      "tags": [],
      "type": "textfield",
      "conditional": {
        "eq": "",
        "when": null,
        "show": ""
      },
      "validate": {
        "customPrivate": false,
        "custom": "",
        "pattern": "([a-z0-9])+",
        "maxLength": 10,
        "minLength": 10,
        "required": true
      },
      "clearOnHide": true,
      "persistent": true,
      "unique": false,
      "protected": false,
      "defaultValue": "",
      "multiple": false,
      "suffix": "",
      "prefix": "",
      "placeholder": "Podaj token",
      "key": "token",
      "label": "Token",
      "inputMask": "",
      "inputType": "text",
      "tableView": true,
      "input": true
    },
    {
      "lockKey": true,
      "conditional": {
        "eq": "",
        "when": null,
        "show": ""
      },
      "tags": [],
      "type": "survey",
      "validate": {
        "customPrivate": false,
        "custom": "",
        "required": true
      },
      "clearOnHide": true,
      "persistent": true,
      "protected": false,
      "defaultValue": "",
      "values": [
        {
          "label": "tragiczny",
          "value": "1"
        },
        {
          "label": "słaby",
          "value": "2"
        },
        {
          "label": "nijaki",
          "value": "3"
        },
        {
          "label": "dobry",
          "value": "4"
        },
        {
          "label": "genialny",
          "value": "5"
        },
        {
          "label": "nie wiem",
          "value": "0"
        }
      ],
      "questions": [
        {
          "label": "Milczenie owiec",
          "value": "milczenieOwiec"
        },
        {
          "label": "Pulp fiction",
          "value": "pulpFiction"
        },
        {
          "label": "Incepcja",
          "value": "incepcja"
        },
        {
          "label": "Matrix",
          "value": "matrix"
        },
        {
          "label": "Władca pierścieni",
          "value": "wladcaPierscieni"
        },
        {
          "label": "Harry Potter",
          "value": "harryPotter"
        }
      ],
      "key": "filmy",
      "label": "Zaznacz jak bardzo podobał ci się film",
      "tableView": true,
      "input": true
    },
    {
      "conditional": {
        "eq": "",
        "when": null,
        "show": ""
      },
      "tags": [],
      "type": "checkbox",
      "validate": {
        "required": false
      },
      "clearOnHide": true,
      "persistent": true,
      "protected": false,
      "defaultValue": false,
      "key": "dodatkowyformularz",
      "datagridLabel": true,
      "label": "Dodatkowy formularz",
      "hideLabel": true,
      "tableView": true,
      "inputType": "checkbox",
      "input": true
    },
    {
      "conditional": {
        "eq": "true",
        "when": "dodatkowyformularz",
        "show": "true"
      },
      "tags": [],
      "type": "survey",
      "validate": {
        "customPrivate": false,
        "custom": "",
        "required": false
      },
      "clearOnHide": true,
      "persistent": true,
      "protected": false,
      "defaultValue": "",
      "values": [
        {
          "label": "nie lubię",
          "value": "1"
        },
        {
          "label": "może być",
          "value": "2"
        },
        {
          "label": "lubię",
          "value": "3"
        }
      ],
      "questions": [
        {
          "label": "Piłka nożna",
          "value": "pilkaNozna"
        },
        {
          "label": "Koszykówka",
          "value": "koszykowka"
        },
        {
          "label": "Pływanie",
          "value": "plywanie"
        },
        {
          "label": "Śiatkówka",
          "value": "siatkowka"
        },
        {
          "label": "Tenis ziemny",
          "value": "tenisZiemny"
        }
      ],
      "key": "wypenijankiet",
      "label": "Wypełnij ankietę",
      "tableView": true,
      "input": true
    },
    {
      "type": "button",
      "theme": "primary",
      "disableOnInvalid": false,
      "action": "submit",
      "block": false,
      "rightIcon": "",
      "leftIcon": "",
      "size": "md",
      "key": "submit",
      "tableView": false,
      "label": "Submit",
      "input": true
    }
  ],
  "owner": "591026c404309f0a5e15049f",
  "submissionAccess": [
    {
      "type": "create_own",
      "roles": [
        "5910268804309f0a5e150492"
      ]
    },
    {
      "type": "read_own",
      "roles": [
        "5910268804309f0a5e150492"
      ]
    },
    {
      "type": "update_own",
      "roles": [
        "5910268804309f0a5e150492"
      ]
    },
    {
      "type": "delete_own",
      "roles": [
        "5910268804309f0a5e150492"
      ]
    }
  ],
  "access": [
    {
      "type": "read_all",
      "roles": [
        "5910268804309f0a5e150491",
        "5910268804309f0a5e150492",
        "5910268804309f0a5e150493"
      ]
    }
  ],
  "tags": [
    "common"
  ],
  "type": "form"
}');

