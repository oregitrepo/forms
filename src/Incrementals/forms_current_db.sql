--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.6
-- Dumped by pg_dump version 9.5.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: dict; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE dict (
    id integer NOT NULL,
    slag character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE dict OWNER TO postgres;

--
-- Name: dict_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dict_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dict_id_seq OWNER TO postgres;

--
-- Name: dict_values; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE dict_values (
    id integer NOT NULL,
    dict_id integer NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE dict_values OWNER TO postgres;

--
-- Name: forms; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE forms (
    id integer NOT NULL,
    report_id integer,
    form json NOT NULL,
    status boolean NOT NULL
);


ALTER TABLE forms OWNER TO postgres;

--
-- Name: forms_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE forms_groups (
    id integer NOT NULL,
    parent_id integer,
    name character varying(255) NOT NULL,
    description text NOT NULL
);


ALTER TABLE forms_groups OWNER TO postgres;

--
-- Name: forms_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE forms_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE forms_groups_id_seq OWNER TO postgres;

--
-- Name: forms_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE forms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE forms_id_seq OWNER TO postgres;

--
-- Name: forms_to_forms_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE forms_to_forms_groups (
    forms_group_id integer NOT NULL,
    form_id integer NOT NULL
);


ALTER TABLE forms_to_forms_groups OWNER TO postgres;

--
-- Name: forms_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE forms_types (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE forms_types OWNER TO postgres;

--
-- Name: forms_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE forms_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE forms_types_id_seq OWNER TO postgres;

--
-- Name: fos_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE fos_group (
    id integer NOT NULL,
    name character varying(180) NOT NULL,
    roles text NOT NULL
);


ALTER TABLE fos_group OWNER TO postgres;

--
-- Name: COLUMN fos_group.roles; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN fos_group.roles IS '(DC2Type:array)';


--
-- Name: fos_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE fos_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE fos_group_id_seq OWNER TO postgres;

--
-- Name: fos_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE fos_user (
    id integer NOT NULL,
    username character varying(180) NOT NULL,
    username_canonical character varying(180) NOT NULL,
    email character varying(180) NOT NULL,
    email_canonical character varying(180) NOT NULL,
    enabled boolean NOT NULL,
    salt character varying(255) DEFAULT NULL::character varying,
    password character varying(255) NOT NULL,
    last_login timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    confirmation_token character varying(180) DEFAULT NULL::character varying,
    password_requested_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    roles text NOT NULL
);


ALTER TABLE fos_user OWNER TO postgres;

--
-- Name: COLUMN fos_user.roles; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN fos_user.roles IS '(DC2Type:array)';


--
-- Name: fos_user_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE fos_user_group (
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE fos_user_group OWNER TO postgres;

--
-- Name: fos_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE fos_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE fos_user_id_seq OWNER TO postgres;

--
-- Name: group_to_forms_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE group_to_forms_groups (
    forms_group_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE group_to_forms_groups OWNER TO postgres;

--
-- Name: institutions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE institutions (
    id integer NOT NULL,
    user_id integer,
    name character varying(255) NOT NULL,
    description character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE institutions OWNER TO postgres;

--
-- Name: institutions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE institutions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE institutions_id_seq OWNER TO postgres;

--
-- Name: reports; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE reports (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    class character varying(255) NOT NULL,
    data json NOT NULL
);


ALTER TABLE reports OWNER TO postgres;

--
-- Name: reports_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reports_id_seq OWNER TO postgres;

--
-- Name: submissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE submissions (
    id integer NOT NULL,
    form_id integer,
    user_id integer,
    token character varying(255) NOT NULL,
    submission json NOT NULL,
    submit_date date NOT NULL
);


ALTER TABLE submissions OWNER TO postgres;

--
-- Name: submissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE submissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE submissions_id_seq OWNER TO postgres;

--
-- Name: tokens; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tokens (
    id integer NOT NULL,
    token character varying(255) NOT NULL,
    institution_id integer,
    gender_id integer NOT NULL,
    province_id integer NOT NULL,
    population_id integer NOT NULL,
    father_education_id integer NOT NULL,
    mother_education_id integer NOT NULL
);


ALTER TABLE tokens OWNER TO postgres;

--
-- Name: tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tokens_id_seq OWNER TO postgres;

--
-- Name: tokens_users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tokens_users (
    token_id integer NOT NULL,
    user_id integer NOT NULL,
    institution_id integer NOT NULL
);


ALTER TABLE tokens_users OWNER TO postgres;

--
-- Data for Name: dict; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY dict (id, slag, name) FROM stdin;
\.


--
-- Name: dict_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dict_id_seq', 1, false);


--
-- Data for Name: dict_values; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY dict_values (id, dict_id, value) FROM stdin;
\.


--
-- Data for Name: forms; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY forms (id, report_id, form, status) FROM stdin;
\.


--
-- Data for Name: forms_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY forms_groups (id, parent_id, name, description) FROM stdin;
\.


--
-- Name: forms_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('forms_groups_id_seq', 1, false);


--
-- Name: forms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('forms_id_seq', 1, false);


--
-- Data for Name: forms_to_forms_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY forms_to_forms_groups (forms_group_id, form_id) FROM stdin;
\.


--
-- Data for Name: forms_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY forms_types (id, name) FROM stdin;
\.


--
-- Name: forms_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('forms_types_id_seq', 1, false);


--
-- Data for Name: fos_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY fos_group (id, name, roles) FROM stdin;
3	Grupa1s	a:13:{i:0;s:10:"VIEW_FORMS";i:1;s:16:"VIEW_SUBMISSIONS";i:2;s:12:"VIEW_REPORTS";i:3;s:10:"VIEW_USERS";i:4;s:11:"VIEW_GROUPS";i:5;s:10:"VIEW_STATS";i:6;s:17:"VIEW_FORMS_GROUPS";i:7;s:15:"VIEW_USER_FORMS";i:8;s:14:"GENERATE_TOKEN";i:9;s:11:"SUBMIT_FORM";i:10;s:12:"MANAGE_TOKEN";i:11;s:17:"VIEW_INSTITUTIONS";i:12;s:10:"VIEW_CARDS";}
\.


--
-- Name: fos_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('fos_group_id_seq', 3, true);


--
-- Data for Name: fos_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY fos_user (id, username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles) FROM stdin;
1	rexuxutu	rexuxutu	jyboqu@gmail.com	jyboqu@gmail.com	t	\N	$2y$13$uZsf09GMthxULyviKDI8HeKsJqWfal6enyOQSCmpVE1112.FJujZy	\N	\N	\N	N;
\.


--
-- Data for Name: fos_user_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY fos_user_group (user_id, group_id) FROM stdin;
\.


--
-- Name: fos_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('fos_user_id_seq', 1, true);


--
-- Data for Name: group_to_forms_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY group_to_forms_groups (forms_group_id, group_id) FROM stdin;
\.


--
-- Data for Name: institutions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY institutions (id, user_id, name, description) FROM stdin;
\.


--
-- Name: institutions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('institutions_id_seq', 1, false);


--
-- Data for Name: reports; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY reports (id, name, class, data) FROM stdin;
\.


--
-- Name: reports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('reports_id_seq', 1, false);


--
-- Data for Name: submissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY submissions (id, form_id, user_id, token, submission, submit_date) FROM stdin;
\.


--
-- Name: submissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('submissions_id_seq', 1, false);


--
-- Data for Name: tokens; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tokens (id, token, institution_id, gender_id, province_id, population_id, father_education_id, mother_education_id) FROM stdin;
\.


--
-- Name: tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tokens_id_seq', 1, false);


--
-- Data for Name: tokens_users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tokens_users (token_id, user_id, institution_id) FROM stdin;
\.


--
-- Name: dict_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dict
    ADD CONSTRAINT dict_pkey PRIMARY KEY (id);


--
-- Name: dict_values_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dict_values
    ADD CONSTRAINT dict_values_pkey PRIMARY KEY (id, dict_id);


--
-- Name: forms_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY forms_groups
    ADD CONSTRAINT forms_groups_pkey PRIMARY KEY (id);


--
-- Name: forms_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY forms
    ADD CONSTRAINT forms_pkey PRIMARY KEY (id);


--
-- Name: forms_to_forms_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY forms_to_forms_groups
    ADD CONSTRAINT forms_to_forms_groups_pkey PRIMARY KEY (forms_group_id, form_id);


--
-- Name: forms_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY forms_types
    ADD CONSTRAINT forms_types_pkey PRIMARY KEY (id);


--
-- Name: fos_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fos_group
    ADD CONSTRAINT fos_group_pkey PRIMARY KEY (id);


--
-- Name: fos_user_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fos_user_group
    ADD CONSTRAINT fos_user_group_pkey PRIMARY KEY (user_id, group_id);


--
-- Name: fos_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fos_user
    ADD CONSTRAINT fos_user_pkey PRIMARY KEY (id);


--
-- Name: group_to_forms_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY group_to_forms_groups
    ADD CONSTRAINT group_to_forms_groups_pkey PRIMARY KEY (forms_group_id, group_id);


--
-- Name: institutions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY institutions
    ADD CONSTRAINT institutions_pkey PRIMARY KEY (id);


--
-- Name: reports_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reports
    ADD CONSTRAINT reports_pkey PRIMARY KEY (id);


--
-- Name: submissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY submissions
    ADD CONSTRAINT submissions_pkey PRIMARY KEY (id);


--
-- Name: tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tokens
    ADD CONSTRAINT tokens_pkey PRIMARY KEY (id);


--
-- Name: tokens_users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tokens_users
    ADD CONSTRAINT tokens_users_pkey PRIMARY KEY (token_id, user_id, institution_id);


--
-- Name: dict_un; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX dict_un ON dict USING btree (slag);


--
-- Name: idx_3f6169f75ff69b7d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_3f6169f75ff69b7d ON submissions USING btree (form_id);


--
-- Name: idx_3f6169f7a76ed395; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_3f6169f7a76ed395 ON submissions USING btree (user_id);


--
-- Name: idx_583d1f3ea76ed395; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_583d1f3ea76ed395 ON fos_user_group USING btree (user_id);


--
-- Name: idx_583d1f3efe54d947; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_583d1f3efe54d947 ON fos_user_group USING btree (group_id);


--
-- Name: idx_7ba1db675ff69b7d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_7ba1db675ff69b7d ON forms_to_forms_groups USING btree (form_id);


--
-- Name: idx_7ba1db67f4f5c075; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_7ba1db67f4f5c075 ON forms_to_forms_groups USING btree (forms_group_id);


--
-- Name: idx_96ebc9d1727aca70; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_96ebc9d1727aca70 ON forms_groups USING btree (parent_id);


--
-- Name: idx_97deb59f10405986; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_97deb59f10405986 ON tokens_users USING btree (institution_id);


--
-- Name: idx_97deb59f41dee7b9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_97deb59f41dee7b9 ON tokens_users USING btree (token_id);


--
-- Name: idx_97deb59fa76ed395; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_97deb59fa76ed395 ON tokens_users USING btree (user_id);


--
-- Name: idx_b5742dfc6ea1cf6e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_b5742dfc6ea1cf6e ON dict_values USING btree (dict_id);


--
-- Name: idx_cb544664a76ed395; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_cb544664a76ed395 ON institutions USING btree (user_id);


--
-- Name: idx_fcedf7d4f4f5c075; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fcedf7d4f4f5c075 ON group_to_forms_groups USING btree (forms_group_id);


--
-- Name: idx_fcedf7d4fe54d947; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fcedf7d4fe54d947 ON group_to_forms_groups USING btree (group_id);


--
-- Name: idx_fd3f1bf74bd2a4c0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fd3f1bf74bd2a4c0 ON forms USING btree (report_id);


--
-- Name: tokens_un; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX tokens_un ON tokens USING btree (token);


--
-- Name: uniq_4b019ddb5e237e06; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uniq_4b019ddb5e237e06 ON fos_group USING btree (name);


--
-- Name: uniq_957a647992fc23a8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uniq_957a647992fc23a8 ON fos_user USING btree (username_canonical);


--
-- Name: uniq_957a6479a0d96fbf; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uniq_957a6479a0d96fbf ON fos_user USING btree (email_canonical);


--
-- Name: uniq_957a6479c05fb297; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uniq_957a6479c05fb297 ON fos_user USING btree (confirmation_token);


--
-- Name: fk_3f6169f75ff69b7d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY submissions
    ADD CONSTRAINT fk_3f6169f75ff69b7d FOREIGN KEY (form_id) REFERENCES forms(id);


--
-- Name: fk_3f6169f7a76ed395; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY submissions
    ADD CONSTRAINT fk_3f6169f7a76ed395 FOREIGN KEY (user_id) REFERENCES fos_user(id);


--
-- Name: fk_583d1f3ea76ed395; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fos_user_group
    ADD CONSTRAINT fk_583d1f3ea76ed395 FOREIGN KEY (user_id) REFERENCES fos_user(id);


--
-- Name: fk_583d1f3efe54d947; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fos_user_group
    ADD CONSTRAINT fk_583d1f3efe54d947 FOREIGN KEY (group_id) REFERENCES fos_group(id);


--
-- Name: fk_7ba1db675ff69b7d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY forms_to_forms_groups
    ADD CONSTRAINT fk_7ba1db675ff69b7d FOREIGN KEY (form_id) REFERENCES forms(id);


--
-- Name: fk_7ba1db67f4f5c075; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY forms_to_forms_groups
    ADD CONSTRAINT fk_7ba1db67f4f5c075 FOREIGN KEY (forms_group_id) REFERENCES forms_groups(id);


--
-- Name: fk_96ebc9d1727aca70; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY forms_groups
    ADD CONSTRAINT fk_96ebc9d1727aca70 FOREIGN KEY (parent_id) REFERENCES forms_groups(id);


--
-- Name: fk_97deb59f10405986; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tokens_users
    ADD CONSTRAINT fk_97deb59f10405986 FOREIGN KEY (institution_id) REFERENCES institutions(id);


--
-- Name: fk_97deb59f41dee7b9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tokens_users
    ADD CONSTRAINT fk_97deb59f41dee7b9 FOREIGN KEY (token_id) REFERENCES tokens(id);


--
-- Name: fk_97deb59fa76ed395; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tokens_users
    ADD CONSTRAINT fk_97deb59fa76ed395 FOREIGN KEY (user_id) REFERENCES fos_user(id);


--
-- Name: fk_b5742dfc6ea1cf6e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dict_values
    ADD CONSTRAINT fk_b5742dfc6ea1cf6e FOREIGN KEY (dict_id) REFERENCES dict(id);


--
-- Name: fk_cb544664a76ed395; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY institutions
    ADD CONSTRAINT fk_cb544664a76ed395 FOREIGN KEY (user_id) REFERENCES fos_user(id);


--
-- Name: fk_fcedf7d4f4f5c075; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY group_to_forms_groups
    ADD CONSTRAINT fk_fcedf7d4f4f5c075 FOREIGN KEY (forms_group_id) REFERENCES forms_groups(id);


--
-- Name: fk_fcedf7d4fe54d947; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY group_to_forms_groups
    ADD CONSTRAINT fk_fcedf7d4fe54d947 FOREIGN KEY (group_id) REFERENCES fos_group(id);


--
-- Name: fk_fd3f1bf74bd2a4c0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY forms
    ADD CONSTRAINT fk_fd3f1bf74bd2a4c0 FOREIGN KEY (report_id) REFERENCES reports(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

