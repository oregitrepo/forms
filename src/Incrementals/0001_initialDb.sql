drop table if exists public.forms;
drop table if exists public.users;
drop table if exists public.tokens;
drop table if exists public.roles;
drop table if exists public.submissions;


CREATE TABLE public.forms (
	id serial NOT NULL,
	form json NOT null,
	CONSTRAINT forms_pk PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE public.users (
	id serial NOT NULL,
	"name" varchar NOT NULL,
	"password" varchar NOT NULL,
	"e-mail" varchar NOT NULL,
	roles json null,
	CONSTRAINT users_pk PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE public.tokens (
	token varchar NOT NULL,
	CONSTRAINT tokens_un UNIQUE (token),
	CONSTRAINT tokens_pk PRIMARY KEY (token)
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE public.roles (
	"name" varchar NOT NULL,
	"privileges" json NOT NULL,
	CONSTRAINT roles_un UNIQUE ("name"),
	CONSTRAINT roles_pk PRIMARY KEY (name)
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE public.submissions (
	id serial NOT NULL,
	token varchar NOT NULL,
	form_id int4 NOT NULL,
	submission json NOT null,
	CONSTRAINT submissions_pk PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;