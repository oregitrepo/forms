DROP TABLE IF EXISTS public.tokens;

CREATE TABLE public.tokens (
	id serial NOT NULL,
	token varchar NOT NULL,
	CONSTRAINT tokens_pk PRIMARY KEY (id),
	CONSTRAINT tokens_un UNIQUE (token)
)
WITH (
	OIDS=FALSE
) ;
