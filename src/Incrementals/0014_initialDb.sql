ALTER TABLE public.submissions ADD user_id int4 NULL ;
UPDATE submissions SET    user_id = 1;
ALTER TABLE public.submissions ALTER COLUMN user_id SET NOT NULL ;