DROP TABLE IF EXISTS public.tokens ;

CREATE TABLE public.tokens (
	id serial NOT NULL,
	token varchar(255) NOT NULL,
	institution_id int4 NULL,
	gender_id int4 NOT NULL,
	province_id int4 NOT NULL,
	population_id int4 NOT NULL,
	father_education_id int4 NOT NULL,
	mother_education_id int4 NOT NULL,
	CONSTRAINT tokens_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;
CREATE UNIQUE INDEX tokens_un ON public.tokens (token DESC) ;

INSERT INTO public.tokens (id,token,institution_id,gender_id,province_id,population_id,father_education_id,mother_education_id) VALUES (
1,'022a010f','1','1','1','1','1','1');
INSERT INTO public.tokens (id,token,institution_id,gender_id,province_id,population_id,father_education_id,mother_education_id) VALUES (
2,'08d1026f','1','1','1','1','1','1');

DROP TABLE IF EXISTS public.dict ;
DROP TABLE IF EXISTS public.dict_values ;

CREATE TABLE public.dict (
	id serial NOT NULL,
	slag varchar NOT NULL,
	"name" varchar NOT NULL,
	CONSTRAINT dict_pk PRIMARY KEY (id),
	CONSTRAINT dict_un UNIQUE (slag)
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE public.dict_values (
	id int4 NOT NULL,
	dict_id int4 NOT NULL,
	value varchar NOT NULL,
	CONSTRAINT dict_values_pk PRIMARY KEY (id,dict_id)
)
WITH (
	OIDS=FALSE
) ;

INSERT INTO public.dict (id,slag,"name") VALUES (
1,'gender','Płeć');
INSERT INTO public.dict (id,slag,"name") VALUES (
2,'province','Województwo');
INSERT INTO public.dict (id,slag,"name") VALUES (
3,'population','Wielkość miejsca zamieszania');
INSERT INTO public.dict (id,slag,"name") VALUES (
4,'education','Wykształcenie');

INSERT INTO public.dict_values (id,dict_id,value) VALUES (
1,2,'mazowieckie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
2,2,'śląskie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
3,2,'wielkopolskie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
4,2,'małopolskie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
5,2,'dolnośląskie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
6,2,'łódzkie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
7,2,'pomorskie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
8,2,'lubelskie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
9,2,'podkarpackie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
10,2,'kujawsko-pomorskie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
11,2,'zachodniopomorskie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
12,2,'warmińsko-mazurskie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
13,2,'świętokrzyskie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
14,2,'podlaskie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
15,2,'lubuskie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
16,2,'opolskie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
1,3,'wieś');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
1,4,'podstawowe');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
4,4,'nieznane');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
3,4,'wyższe');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
2,4,'średnie');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
2,3,'miasto');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
1,1,'chłopiec ');
INSERT INTO public.dict_values (id,dict_id,value) VALUES (
2,1,'dziewczyna');

