ALTER TABLE public.forms ADD status bool NULL ;
UPDATE forms SET    status = TRUE;
ALTER TABLE public.forms ALTER COLUMN status SET NOT NULL ;

