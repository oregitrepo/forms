DROP TABLE IF EXISTS public.fos_user;
CREATE TABLE public.fos_user (
	id int4 NOT NULL,
	username varchar(180) NOT NULL,
	username_canonical varchar(180) NOT NULL,
	email varchar(180) NOT NULL,
	email_canonical varchar(180) NOT NULL,
	enabled bool NOT NULL,
	salt varchar(255) NULL DEFAULT NULL::character varying,
	password varchar(255) NOT NULL,
	last_login timestamp NULL DEFAULT NULL::timestamp without time zone,
	confirmation_token varchar(180) NULL DEFAULT NULL::character varying,
	password_requested_at timestamp NULL DEFAULT NULL::timestamp without time zone,
	roles text NOT NULL,
	CONSTRAINT fos_user_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;
CREATE UNIQUE INDEX uniq_957a647992fc23a8 ON public.fos_user (username_canonical DESC) ;
CREATE UNIQUE INDEX uniq_957a6479a0d96fbf ON public.fos_user (email_canonical DESC) ;
CREATE UNIQUE INDEX uniq_957a6479c05fb297 ON public.fos_user (confirmation_token DESC) ;

