DROP TABLE IF EXISTS public.fos_user_group;
DROP TABLE IF EXISTS public.fos_group;
CREATE TABLE public.fos_group (
	id int4 NOT NULL,
	"name" varchar(180) NOT NULL,
	roles text NOT NULL,
	CONSTRAINT fos_group_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;
CREATE UNIQUE INDEX uniq_4b019ddb5e237e06 ON public.fos_group (name DESC) ;


CREATE TABLE public.fos_user_group (
	user_id int4 NOT NULL,
	group_id int4 NOT NULL,
	CONSTRAINT fos_user_group_pkey PRIMARY KEY (user_id,group_id),
	CONSTRAINT fk_583d1f3ea76ed395 FOREIGN KEY (user_id) REFERENCES public.fos_user(id),
	CONSTRAINT fk_583d1f3efe54d947 FOREIGN KEY (group_id) REFERENCES public.fos_group(id)
)
WITH (
	OIDS=FALSE
) ;
CREATE INDEX idx_583d1f3ea76ed395 ON public.fos_user_group (user_id DESC) ;
CREATE INDEX idx_583d1f3efe54d947 ON public.fos_user_group (group_id DESC) ;

