DROP TABLE IF EXISTS public.tokens_users;

CREATE TABLE public.tokens_users (
	token_id int4 NOT NULL,
	user_id int4 NOT NULL,
	institution_id int4 NOT NULL,
	CONSTRAINT tokens_users_pk PRIMARY KEY (token_id,user_id, institution_id)
)
WITH (
	OIDS=FALSE
) ;

DROP TABLE IF EXISTS public.institutions;

CREATE TABLE public.institutions (
	id serial NOT NULL,
	"name" varchar NOT NULL,
	description varchar NULL,
	user_id varchar NOT NULL,
	CONSTRAINT institutions_pk PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;