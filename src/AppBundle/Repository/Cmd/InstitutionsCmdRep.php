<?php

namespace AppBundle\Repository\Cmd;

use AppBundle\Entity\Institutions;
use Doctrine\ORM\EntityManager;

class InstitutionsCmdRep
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function addInstitution($name, $description, $userId)
    {
        try {
            $institution = (new Institutions())->setName($name)->setDescription($description)->setUserId($userId);
            $this->em->persist($institution);
            $this->em->flush();
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function deleteInstitutionById($id)
    {
        try {
            $institution = $this->em->getRepository('AppBundle:Institutions')->findOneBy(['id' => $id]);
            $this->em->remove($institution);
            $this->em->flush();
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function updateInstitution($id, $name, $description, $userId)
    {
        try {
            $institution = $this->em->getRepository('AppBundle:Institutions')->findOneBy(['id' => $id]);
            $institution->setName($name);
            $institution->setDescription($description);
            $institution->setUserId($userId);
            $this->em->flush();
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}