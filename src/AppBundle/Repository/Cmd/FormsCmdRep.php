<?php

namespace AppBundle\Repository\Cmd;

use AppBundle\Entity\Forms;
use Doctrine\ORM\EntityManager;

class FormsCmdRep
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function addNewForm($form, $reportId)
    {
        try {
            $createForm = new Forms();
            $createForm->setForm($form);
            $createForm->setReportId($reportId);
            $createForm->setStatus(true);
            $this->em->persist($createForm);
            $this->em->flush();
            return $createForm->getId();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function setStatus($formId)
    {
        try {
            $form = $this->em->getRepository('AppBundle:Forms')->findOneById($formId);
            $form->setStatus(!$form->isStatus());
            $this->em->flush();
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function setTitle($id, $title)
    {
        try {
            $rep = $this->em->getRepository('AppBundle:Forms');
            $form = $rep->findOneById($id);
            $formValue = $form->getForm();
            if (is_string($formValue)) {
                $formValue = json_decode($formValue, true);
            }
            $formValue['title'] = $title;
            $formValue['modified'] = new \DateTime();
            $form->setForm($formValue);
            $this->em->flush();
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function setReport($formId, $reportId)
    {
        try {
            $form = $this->em->getRepository('AppBundle:Forms')->findOneById($formId);
            $form->setReportId($reportId);
            $this->em->persist($form);
            $this->em->flush();
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}