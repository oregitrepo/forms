<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 17.05.17
 * Time: 12:00
 */

namespace AppBundle\Repository\Cmd;


use AppBundle\Entity\FormsToFormsGroups;
use Doctrine\ORM\EntityManager;

class FormsToFormsGroupsCmdRep
{
    private $em;
    private $repository;

    /**
     * FormsGroupsQueryRep constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('AppBundle:FormsToFormsGroups');
    }

    public function deleteForm($formId, $formsGroupId)
    {
        try {
            $element = $this->repository->findOneBy([
                'formsId' => $formId,
                'formsGroupId' => $formsGroupId
            ]);
            $this->em->remove($element);
            $this->em->flush();
            return 'Usunieto formularz';
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function addForm($formId, $formsGroupId)
    {
        if ($this->repository->findOneBy(['formsId' => $formId, 'formsGroupId' => $formsGroupId])) {
            return 'Formularz już istnieje';
        }
        try {
            $form = new FormsToFormsGroups();
            $form->setFormsGroupId($formsGroupId);
            $form->setFormsId($formId);
            $this->em->persist($form);
            $this->em->flush();
            return 'Dodano formularz';
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function addFormToGroups($formId, $formsGroupId)
    {
        foreach ($formsGroupId as $formGroupId) {
            if ($this->repository->findOneBy(['formsId' => $formId, 'formsGroupId' => $formGroupId])) {
                return 'Formularz już istnieje';
            }
            try {
                $form = new FormsToFormsGroups();
                $form->setFormsGroupId($formGroupId);
                $form->setFormsId($formId);
                $this->em->persist($form);
                $this->em->flush();
            } catch (\Exception $e) {
                throw $e;
            }
        }
    }

    public function getFormsGroupsByFormId($formId)
    {
        $groups = $this->repository->findByFormsId($formId);
        $groupsId = [];
        foreach ($groups as $group) {
            $groupsId[] = $group->getFormsGroupId();
        }
        return $groupsId;
    }

    public function updateFormsGroupsToForm($formId, $formsGroupId)
    {
        $this->em->getConnection()->beginTransaction();
        try {
            foreach ($formsGroupId as $key => $formGroupId) {

                if ($this->repository->findOneBy(['formsId' => $formId, 'formsGroupId' => $formGroupId]) === null) {
                    $newForm = new FormsToFormsGroups();
                    $newForm->setFormsGroupId($formGroupId);
                    $newForm->setFormsId($formId);
                    $this->em->persist($newForm);
                    $this->em->flush();
                }

                $form = $this->repository->findOneBy(['formsId' => $formId, 'formsGroupId' => $formGroupId]);
                $form->setFormsGroupId($formGroupId);
                $form->setFormsId($formId);
                $this->em->flush();
            }

            foreach ($this->repository->findByFormsId($formId) as $key => $formGroupId) {
                if (in_array($formGroupId->getFormsGroupId(), $formsGroupId) === false) {
                    $this->em->remove($formGroupId);
                };
            }
            $this->em->flush();
            $this->em->getConnection()->commit();
            return 'Aktualizowanie wyników';
        } catch (\Exception $e) {
            $this->em->getConnection()->rollback();
            throw $e;
        }
    }
}