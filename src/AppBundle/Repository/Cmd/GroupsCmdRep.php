<?php

namespace AppBundle\Repository\Cmd;

use AppBundle\Entity\Group;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\GroupManager;

class GroupsCmdRep
{
    private $gm;
    private $em;
    private $repository;

    public function __construct(GroupManager $gm, EntityManager $em)
    {
        $this->gm = $gm;
        $this->em = $em;
        $this->repository = $this->em->getRepository('AppBundle:Group');
    }

    public function createGroup($name, $roles)
    {
        try {
            $group = (new Group($name))->setRoles(array_values($roles));
            $this->em->persist($group);
            $this->em->flush();
            return "Utoworzono grupę:  $name";
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function updateGroup($id, $name, $roles)
    {
        try {
            $group = $this->repository->findOneBy(['id' => $id]);
            $oldName = $group->getName();
            $group->setName($name);
            $group->setRoles(array_values($roles));
            $this->em->flush();
            return "Zaktualizowano grupę: $oldName";
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function deleteGroup($id)
    {
        try {
            $this->deleteGroupConnections($id);
            $group = $this->repository->findOneBy(['id' => $id]);
            $name = $group->getName();
            $this->gm->deleteGroup($group);
            return "Usunięto grupę: $name";
        } catch (\Exception $e) {
            throw $e;
        }
    }

    private function deleteGroupConnections($groupId)
    {
        try {
            $this->em->createQuery('DELETE FROM AppBundle:FosUserGroup
        WHERE group_id = :group_id')->setParameter('group_id', $groupId);
            return "Połączenia usunięte";
        } catch (\Exception $e) {
            throw $e;
        }
    }
}