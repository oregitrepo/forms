<?php

namespace AppBundle\Repository\Cmd;

use AppBundle\Entity\Submissions;
use Doctrine\ORM\EntityManager;

class SubmissionsCmdRep
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function submitForm($token, $userId, $formId, $submission)
    {
        try {
            $newSubmission = (new Submissions())->setToken($token)->setUserId($userId)->setFormId($formId)->setSubmission($submission)->setSubmitDate(new \DateTime("now"));
            $this->em->persist($newSubmission);
            $this->em->flush();
        }catch(\Exception $e){
            return false;
        }

        return true;
    }

    public function updateSubmission($submissionId, $updateData)
    {
        try {
            $submission = $this->em->getRepository('AppBundle:Submissions')->findOneById($submissionId);
            $submission->setSubmission($updateData)->setSubmitDate(new \DateTime("now"));
            $this->em->flush();
        } catch(\Exception $e){
            return false;
        }

        return true;
    }

    public function deleteSubmissionById($id)
    {
        try {
            $submission = $this->em->getRepository('AppBundle:Submissions')->findOneById($id);
            $this->em->remove($submission);
            $this->em->flush();
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}