<?php

namespace AppBundle\Repository\Cmd;

use AppBundle\Entity\Tokens;
use Doctrine\ORM\EntityManager;

class TokensCmdRep
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function addNewToken($token, $genderId, $provinceId, $populationId, $fatherEducationId, $motherEducationId)
    {
        try {
            $newToken = (new Tokens())
                ->setToken($token)
                ->setGenderId($genderId)
                ->setProvinceId($provinceId)
                ->setPopulationId($populationId)
                ->setFatherEducationId($fatherEducationId)
                ->setMotherEducationId($motherEducationId);
            $this->em->persist($newToken);
            $this->em->flush();
            return "Token: $token został dodany";
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function assignTokenToInstitution($tokenId, $institutionId)
    {
        try {
            $token = $this->em->getRepository('AppBundle:Tokens')->findOneBy(['id' => $tokenId]);
            $token->setInstitutionId($institutionId);
            $tokenName = $token->getToken();
            $this->em->flush();
            return "Token: $tokenName został przypisany";
        } catch (\Exception $e) {
            throw $e;
        }
    }
}