<?php

namespace AppBundle\Repository\Cmd;

use AppBundle\Entity\FormsGroups;
use Doctrine\ORM\EntityManager;

class FormsGroupsCmdRep
{
    private $em;
    private $repository;

    /**
     * FormsGroupsQueryRep constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('AppBundle:FormsGroups');

    }

    public function editFormsGroup($id, $name, $description)
    {
        try {
            $formsGroup = $this->repository->findOneBy(['id' => $id]);
            $formsGroup->setName($name);
            $formsGroup->setDescription($description);
            $this->em->flush();
            return 'Grupa formularzy została zaktualizowana';
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function deleteFormsGroupById($id)
    {
        try {
            $formsGroup = $this->repository->findOneBy(['id' => $id]);
            $this->em->remove($formsGroup);
            $this->em->flush();
            return 'Grupa formularzy została usunięta';
        } catch (\Exception $e) {
            throw $e;
        }
    }


    public function addFormsGroup($name, $description, $parentId, $level)
    {
        if (!$name) {
            return 'Nazwa nie może być pusta';
        }
        try {
            $formsGroup = new FormsGroups();
            $formsGroup->setName($name);
            $formsGroup->setDescription($description);
            $formsGroup->setParentId($parentId);
            $formsGroup->setLevel($level);
            $this->em->persist($formsGroup);
            $this->em->flush();
            return 'Grupa została dodana';
        } catch (\Exception $e) {
            throw $e;
        }
    }
}