<?php

namespace AppBundle\Repository\Cmd;

use AppBundle\Entity\FormsGroupsToGroup;
use Doctrine\ORM\EntityManager;

class FormsGroupsToGroupCmdRep
{
    private $em;
    private $repository;

    /**
     * FormsGroupsToGroupCmdRep constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('AppBundle:FormsGroupsToGroup');
    }

    public function deleteGroup($groupId, $formsGroupId)
    {
        try {
            $element = $this->repository->findOneBy([
                'groupId' => $groupId,
                'formsGroupId' => $formsGroupId,
            ]);
            $this->em->remove($element);
            $this->em->flush();
            return 'Usunięto grupę';
        } catch (\Exception $e) {
            throw $e;
        }
    }


    public function addGroup($groupId, $formsGroupId)
    {
        if ($this->repository->findOneBy(['groupId' => $groupId, 'formsGroupId' => $formsGroupId])) {
            return 'Grupa już istnieje';
        }
        try {
            $group = new FormsGroupsToGroup();
            $group->setFormsGroupId($formsGroupId);
            $group->setGroupId($groupId);
            $this->em->persist($group);
            $this->em->flush();
            return 'Dodano grupę';
        } catch (\Exception $e) {
            throw $e;
        }
    }
}