<?php

namespace AppBundle\Repository\Cmd;

use AppBundle\Entity\TokensUsers;
use Doctrine\ORM\EntityManager;

class TokensUsersCmdRep
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function addConnection($tokenId, $userId, $institutionId)
    {
        try {
            $connection = (new TokensUsers())->setTokenId($tokenId)->setUserId($userId)->setInstitutionId($institutionId);
            $this->em->persist($connection);
            $this->em->flush();
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function deleteConnection($tokenId, $userId, $institutionId)
    {
        try {
            $connection = $this->em->getRepository('AppBundle:TokensUsers')
                ->findOneBy(['tokenId' => $tokenId, 'userId' => $userId, 'institutionId' => $institutionId]);
            $this->em->remove($connection);
            $this->em->flush();
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}