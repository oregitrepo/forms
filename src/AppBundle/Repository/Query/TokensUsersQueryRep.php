<?php

namespace AppBundle\Repository\Query;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class TokensUsersQueryRep
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getConnectionsByTokenId($tokenId)
    {
        try {
            return $this->em->getRepository('AppBundle:TokensUsers')->createQueryBuilder('tu')
                ->select('tu.userId')
                ->where('tu.tokenId = :tokenId')
                ->setParameter('tokenId', $tokenId)
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}