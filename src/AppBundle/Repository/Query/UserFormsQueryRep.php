<?php
/**
 * Created by PhpStorm.
 * User: Martino
 * Date: 20.05.2017
 * Time: 13:33
 */

namespace AppBundle\Repository\Query;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class UserFormsQueryRep
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getFormsGroupsByUserId($userId)
    {
        try {
            $result = $this->em->createQuery(
                "SELECT fg FROM 
                 AppBundle:FosUser as u 
                 JOIN u.groups as g
                 JOIN AppBundle:FormsGroupsToGroup as fgtg
                 WITH g.id = fgtg.groupId
                 JOIN AppBundle:FormsGroups as fg
                 WITH fgtg.formsGroupId = fg.id
                 WHERE u.id = :userId")
                ->setParameter('userId', $userId)
                ->getResult(Query::HYDRATE_ARRAY);
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}