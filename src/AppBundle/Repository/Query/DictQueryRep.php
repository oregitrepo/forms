<?php

namespace AppBundle\Repository\Query;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class DictQueryRep
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getAllDicts()
    {
        try {
            return $this->em->getRepository('AppBundle:Dict')->createQueryBuilder('d')
                ->select('d')
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getDictValuesByDictSlag($slag)
    {
        try {
            return $this->em->createQuery('
        SELECT dv 
        FROM AppBundle:DictValues AS dv
        JOIN AppBundle:Dict AS d
        WITH d.id = dv.dict_id
        WHERE d.slag = :slag
        ')->setParameter('slag', $slag)->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}