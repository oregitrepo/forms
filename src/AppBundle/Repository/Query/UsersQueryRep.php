<?php

namespace AppBundle\Repository\Query;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class UsersQueryRep
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getUsersByGroupName($groupName)
    {
        try {
            return $this->em->createQuery('
                SELECT fu.id, fu.username
                FROM AppBundle:FosUser AS fu
                JOIN AppBundle:FosUserGroup AS fug
                WHEN fug.userId = fu.id
                JOIN AppBundle:Group AS g 
                WHEN fug.groupId = g.id
                WHERE g.name = :groupName')
                ->setParameter('groupName', $groupName)
                ->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getUsersWithoutCertainGroup($groupName)
    {
        try {
            return $this->em->createQuery('
                SELECT fu.id, fu.username
                FROM AppBundle:FosUser AS fu
                JOIN AppBundle:FosUserGroup AS fug
                WHEN fug.userId = fu.id
                JOIN AppBundle:Group AS g 
                WHEN fug.groupId = g.id
                WHERE g.name != :groupName')
                ->setParameter('groupName', $groupName)
                ->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getUsersByGroupId($groupId)
    {
        try {
            return $this->em->createQuery('
                SELECT fu.id, fu.username
                FROM AppBundle:FosUser AS fu
                JOIN AppBundle:FosUserGroup AS fug
                WHEN fug.userId = fu.id
                JOIN AppBundle:Group AS g 
                WHEN fug.groupId = g.id
                WHERE g.id = :groupId')
                ->setParameter('groupId', $groupId)
                ->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }


}