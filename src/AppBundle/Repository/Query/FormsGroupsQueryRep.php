<?php

namespace AppBundle\Repository\Query;

use Doctrine\ORM\EntityManager;

class FormsGroupsQueryRep
{
    private $repository;

    public function __construct(EntityManager $em)
    {
        $this->repository = $em->getRepository('AppBundle:FormsGroups');
    }

    public function getAllFormsGroups()
    {
        try {
            $result = $this->repository->createQueryBuilder('fg')
                ->select('fg')
                ->addOrderBy('fg.id', 'ASC')
                ->getQuery()
                ->getArrayResult();
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getFormsGroupsByParentId($id)
    {
        try {
            $result = $this->repository->createQueryBuilder('fg')
                ->select('fg')
                ->where('fg.parentId=:id')
                ->addOrderBy('fg.id', 'ASC')
                ->setParameter('id', $id)
                ->getQuery()
                ->getArrayResult();
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getFormsGroupById($id)
    {
        try {
            return $this->repository->findOneBy([
                'id' => $id
            ]);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
