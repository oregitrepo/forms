<?php

namespace AppBundle\Repository\Query;


use Doctrine\ORM\EntityManager;

class FormsGroupsToGroupQueryRep
{
    private $repository;

    public function __construct(EntityManager $em)
    {
        $this->repository = $em->getRepository('AppBundle:FormsGroupsToGroup');

    }

    public function getGroupsByFormsGroupId($id)
    {
        try {
            return $this->repository->createQueryBuilder('fgtg')
                ->select('fgtg')
                ->where('fgtg.formsGroupId = :id')
                ->setParameter('id', $id)
                ->addOrderBy('fgtg.id', 'ASC')
                ->getQuery()
                ->getArrayResult();
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
