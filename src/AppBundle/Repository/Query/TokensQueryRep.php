<?php

namespace AppBundle\Repository\Query;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class TokensQueryRep
{
    private $em;
    private $repository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('AppBundle:Tokens');
    }

    public function getTokenByToken($token)
    {
        try {
            return $this->repository->createQueryBuilder('t')
                ->select('t')
                ->where('t.token = :token')
                ->setParameter(':token', $token)
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getAllTokens()
    {
        try {
            return $this->repository->createQueryBuilder('t')
                ->select('t')
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getTokensByUserId($userId)
    {
        try {
            return $this->em->createQuery('
                SELECT t FROM AppBundle:Tokens AS t
                JOIN AppBundle:Institutions AS i
                WHEN i.id = t.institutionId
                WHERE i.userId = :userId')
                ->setParameter('userId', $userId)
                ->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getTokenById($id)
    {
        try {
            return $this->repository->createQueryBuilder('t')
                ->select('t')
                ->where('t.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getTokenAssignedToUser($token, $userId, $institutionId)
    {
        try {
            $result = $this->em->createQuery('
                SELECT DISTINCT t.token FROM AppBundle:Tokens AS t
                JOIN AppBundle:Institutions AS i
                WHEN i.id = t.institutionId
                JOIN AppBundle:TokensUsers AS tu
                WHEN t.id = tu.tokenId 
                WHERE t.token = :token AND tu.userId = :userId AND tu.institutionId = :institutionId')
                ->setParameter('userId', $userId)
                ->setParameter('token', $token)
                ->setParameter('institutionId', $institutionId)
                ->getSingleResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $exception) {
            $result = [];
        }
        return $result;
    }
}