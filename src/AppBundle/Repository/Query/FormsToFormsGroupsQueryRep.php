<?php

namespace AppBundle\Repository\Query;

use Doctrine\ORM\EntityManager;

class FormsToFormsGroupsQueryRep
{
    private $repository;

    public function __construct(EntityManager $em)
    {
        $this->repository = $em->getRepository('AppBundle:FormsToFormsGroups');

    }

    public function getFormsByFormsGroupId($id)
    {
        try {
            return $this->repository->createQueryBuilder('ftfg')
                ->select('ftfg')
                ->where('ftfg.formsGroupId = :id')
                ->setParameter('id', $id)
                ->addOrderBy('ftfg.id', 'ASC')
                ->getQuery()
                ->getArrayResult();
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
