<?php

namespace AppBundle\Repository\Query;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class FormsQueryRep
{
    private $em;
    private $repository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('AppBundle:Forms');
    }

    public function getFormById($id)
    {
        try {
            $result = $this->repository->createQueryBuilder('f')
                ->where('f.id= :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            $result = [];
        }
        return $result;
    }

    public function getAllForms()
    {
        try {
            return $this->repository->createQueryBuilder('f')
                ->select('f')
                ->addOrderBy('f.id', 'ASC')
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getAllActiveForms()
    {
        try {
            return $this->repository->createQueryBuilder('f')
                ->select('f')
                ->where('f.status=true')
                ->addOrderBy('f.id', 'ASC')
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getFormsWithSubmissions()
    {
        try {
            return $this->em->createQuery('SELECT f
            FROM AppBundle:Forms AS f
            JOIN AppBundle:Submissions AS s
            WITH f.id = s.formId
            WHERE s.id IS NOT NULL
            GROUP BY f.id
            ORDER BY f.id ASC')->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}