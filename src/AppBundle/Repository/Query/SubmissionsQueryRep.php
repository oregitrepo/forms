<?php
/**
 * Created by PhpStorm.
 * User: mateusz
 * Date: 10.05.17
 * Time: 11:17
 */

namespace AppBundle\Repository\Query;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class SubmissionsQueryRep
{
    private $conn;
    private $em;
    private $queryBuilder;

    public function __construct(Connection $conn, EntityManager $em)
    {
        $this->conn = $conn;
        $this->em = $em;
        $this->queryBuilder = $this->em->createQueryBuilder();
    }

    public function getAllSubmissions()
    {
        try {
            $rep = $this->em->getRepository('AppBundle:Submissions');
            $result = $rep->createQueryBuilder('s')
                ->addOrderBy('s.id', 'ASC')
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
            return $result;
        }catch (\Exception $e){
            throw $e;
        }
    }

    public function getSubmissionById($id)
    {
        try{
            $rep = $this->em->getRepository('AppBundle:Submissions');
            $result = $rep->createQueryBuilder('s')
                ->where('s.id = :id')
                ->addOrderBy('s.id', 'ASC')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleResult(Query::HYDRATE_ARRAY);
            return $result;
        }catch (\Exception $e){
            throw $e;
        }

    }

    public function getSubmissionByFormId($formId)
    {
        try{
            $rep = $this->em->getRepository('AppBundle:Submissions');
            $result = $rep->createQueryBuilder('s')
                ->where('s.formId = :formId')
                ->addOrderBy('s.id', 'ASC')
                ->setParameter('formId', $formId)
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
            return $result;
        }catch (\Exception $e){
            throw $e;
        }
    }

    public function getSubmissionsByTokenAndUserId($token, $userId)
    {
        try{
            $rep = $this->em->getRepository('AppBundle:Submissions');
            $result = $rep->createQueryBuilder('s')
                ->where('s.token = :token AND s.userId = :userId')
                ->addOrderBy('s.id', 'ASC')
                ->setParameter('token', $token)
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
            return $result;
        }catch (\Exception $e){
            throw $e;
        }
    }
}