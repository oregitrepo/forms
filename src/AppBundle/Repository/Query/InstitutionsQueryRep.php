<?php

namespace AppBundle\Repository\Query;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class InstitutionsQueryRep
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getAllInstitutions()
    {
        try {
            return $this->em->getRepository('AppBundle:Institutions')->createQueryBuilder('i')
                ->select('i')
                ->orderBy('i.name', 'ASC')
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getInstitutionById($id)
    {
        try {
            return $this->em->getRepository('AppBundle:Institutions')->createQueryBuilder('i')
                ->select('i')
                ->where('i.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getInstitutionByToken($token)
    {
        try {
            return $this->em->createQuery('
        SELECT i.id FROM AppBundle:Institutions AS i
        JOIN AppBundle:Tokens AS t
        WHEN t.institutionId = i.id
        WHERE t.token = :token
        ')->setParameter('token', $token)
                ->getSingleResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}