<?php

namespace AppBundle\Repository\Query;

use AppBundle\Reports\ReportDataContainer;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class ReportsDataQueryRep
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getReportData($submissionId)
    {
        $submission = $this->em->getRepository('AppBundle:Submissions')->findOneBy(['id' => $submissionId]);
        $form = $this->em->getRepository('AppBundle:Forms')->findOneBy(['id' => $submission->getFormId()]);
        $report = $this->em->getRepository('AppBundle:Reports')->findOneBy(['id' => $form->getReportId()]);
        return new ReportDataContainer($report, $submission, $form);
    }

    public function getAllReports($token)
    {
        return $this->em->createQuery('SELECT r.name, f, s.submitDate, s.id
            FROM AppBundle:Reports AS r
            JOIN AppBundle:Forms AS f
            WITH r.id = f.reportId
            JOIN AppBundle:Submissions AS s
            WITH s.formId = f.id
            WHERE s.token = :token
            ORDER BY f.id ASC')
            ->setParameter('token', $token)
            ->getResult(Query::HYDRATE_ARRAY);
    }

    public function getReports()
    {
        $reports = $this->em->createQuery('SELECT r.id,r.name
        From AppBundle:Reports as r')
            ->getArrayResult();
        return $reports;
    }
}