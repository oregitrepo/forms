<?php

namespace AppBundle\Repository\Query;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class GroupsQueryRep
{
    private $repository;

    public function __construct(EntityManager $em)
    {
        $this->repository = $em->getRepository('AppBundle:Group');
    }

    public function getAllGroups()
    {
        try {
            return $this->repository->createQueryBuilder('g')
                ->select('g')
                ->addOrderBy('g.id', 'ASC')
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getGroupById($id)
    {
        try {
            return $this->repository->createQueryBuilder('g')
                ->select('g')
                ->where('g.id = :id')
                ->setParameter(':id', $id)
                ->addOrderBy('g.id', 'ASC')
                ->getQuery()
                ->getSingleResult(Query::HYDRATE_ARRAY);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}