/**
 * Created by michal on 12.05.17.
 */

Vue.component('survey-field', {
    props: ['field', 'item'],
    template: `
        <div>
            <div v-for="value, key in item.submission[field]">
            <b v-text="item.labels[field].questions[key]"></b><span>: </span>
            <span v-text="item.labels[field].values[value]"></span>
            </div>
        </div>
    `
});

var app = new Vue({
    el: '#root_app',
    data: {
        submissions:[],
        token: $.cookie('token'),
        error: false
    },

    methods: {
        editSubmission: function(id)
        {
            window.location.href = Routing.generate('edit_submission_view', {'submissionId' : id});
        },
        deleteSubmission: function (formId) {
            var token = this.token;
            var urlDelete = Routing.generate('delete_submission');

                $.ajax({
                    method:"POST",
                    url:urlDelete,
                    dataType:"json",
                    data:{"formId":formId,
                        "token": token}
                })
                    .done(function (data) {
                        if(data['delete']) {
                            app.submissions = data['submissions'];
                            toastr.success('Wynik wysłanego formularza został usunięty');
                        }
                        else{
                            toastr.error('Nie udało się usunąć wyniku');
                        }
                    })
                    .fail(function (data) {
                        toastr.error('Wystąpił błąd');
                    });
            }
    },

    mounted: function () {
        var token = this.token;
        $.ajax({
            type: 'POST',
            url: Routing.generate('get_submissions'),
            data: { token: token },
            success: function (data) {
                app.submissions = data;
            }
        })
    },
    delimiters: ['@{', '}']
});