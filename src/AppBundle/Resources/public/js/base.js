$(document).ready(function () {


    if ($.cookie('token')) {
        $('#token-cookie').val($.cookie('token'))
    }

    $(document).on('click', '#token-cookie-set', function () {
        $.ajax({
            type: "POST",
            url: Routing.generate('token_cookie_set'),
            data: {token: $('#token-cookie').val()}
        })
            .done(function (data) {
                var response = JSON.parse(data);
                if (response === false) {
                    toastr.error('Podany token nie istnieje', 'Błąd!');
                } else {
                    var token = $('#token-cookie').val();
                    if (!!$.cookie('token')) {
                        $.removeCookie("token", {path: '/'});
                        $.cookie("token", token, {expires: 3, path: '/'});
                    } else {
                        $.cookie("token", token, {expires: 3, path: '/'});
                    }
                    toastr.success('Token: ' + token + ' został zapisany', 'Sukces!');
                }
            })
            .fail(function () {
                toastr.error('Problem z połączeniem', 'Błąd!');
            });
    });

    $(document).on('click', '#token-cookie-delete', function () {
        if (!!$.cookie('token')) {
            var token = $.cookie('token');
            $.removeCookie("token", {path: '/'});
            $('#token-cookie').val('');
            toastr.success('Token: ' + token + ' został usunięty', 'Sukces!');
        } else {
            toastr.error('Rzaden token nie jest zapisany', 'Błąd!');
        }
    });
});

toastr.options.positionClass = "toast-bottom-right";
