window.onload = function () {

    $('form').submit(function (event) {
        event.preventDefault();
        formJson = $('textarea').val();

        var formGroup = app.formGroup.id;
        if (formGroup[0]) {

        var url = Routing.generate('create_form');

            $.ajax({
                type: "POST",
                url: url,
                data: {"reportId":app.reportId,
                    "formJson": formJson},
                DataType: "json"
            })
                .done(function (data) {
                    app.addFormToGroups(data['id'],formGroup);
                    if(data['statement']){
                        toastr.success('Zapisano formularz w bazie');
                    }else{
                        toastr.error('Podany ciąg znaków nie jest w formacie JSON');
                    }
                })
                .fail(function () {
                    toastr.success('Coś poszło nie tak');
                });
        }
        else
        {
            toastr.error('Nie wybrałeś żadnej grupy formularzy');
        }
    });

    Vue.component('branch',{
        template:`
        <li id="diagnostic-forms">
        
            <input v-if="model.children == undefined" type="checkbox" :id="model.name" :value="model.id" v-model="check" @change="$emit('checked',check)">

            <span @click="toggleFolder" class="clickable" v-if="model.children != undefined">
                <span class="btn-sm btn-info" :class="toggleButtonClass" :title="toggleTitle"></span>
            </span>
            
            <label :for="model.name" class="group"><b>{{ model.name }}</b> </label>       
            
            <ul v-if="visible">
                <branch class="item" v-for="group, index in model.children" :model="group" :index="index" :parent="model.children" :check="check" @checked="checked"></branch>
            </ul>    
        </li>  
        `,
        props:['model','index','parent','check'],
        data:function () {
            return {
                visible:false
            };
        },
        computed:{
            toggleButtonClass:function () {
                return this.visible ? 'btn-sm btn-info glyphicon glyphicon-chevron-up' : 'btn-sm btn-info glyphicon glyphicon-chevron-down';
            },
            toggleTitle:function () {
                return this.visible ? 'Zwiń grupę' : 'Rozwiń grupę';
            }
        },
        methods:{
            toggleFolder:function () {
                this.visible = !this.visible;
            },
            checked:function (data) {
                  this.$emit('checked',data);
            }
        }
    });

    var app = new Vue({
        delimiters: ['@{', '}'],
        el:'#root_app',
        data:{
            formsGroups: [],
            formGroup: {
                id:[]
            },
            reports:[],
            reportId:1
        },
        methods:{
            select:function (data) {
              this.formGroup.id = data;
            },
            getGroupsData:function () {
                url = Routing.generate('get_user_forms');
                $.ajax({
                    url:url,
                    success:function (data) {
                        app.formsGroups = data;
                    }
                });
            },
            addFormToGroups:function(formId,formGroup){
                var url = Routing.generate('add_form_to_forms_group');
                $.ajax({
                    method:"POST",
                    url:url,
                    data:{"formsGroupId":formGroup,
                        "selectedForm":formId
                    },
                    success:function (data) {
                    },
                    error:function (data) {
                    }
                });
            },
            getReportsData:function()
            {
                var url = Routing.generate('reports_get');
                $.ajax({
                    method:"POST",
                    url:url,
                    success:function (data) {
                        app.reports = data;
                    }
                });
            }
        },
        mounted:function () {
            this.getGroupsData();
            this.getReportsData();
        }
    });

}