$(document).ready(function () {

    $(document).on('click', '#create', function () {
        $.ajax({
            type: "POST",
            url: Routing.generate('institutions_add'),
            data: $('#create-form').serialize(),
            dataType: "json"
        })
            .done(function (data) {
                if (data === true) {
                    toastr.success('Poradnia została dodana', 'Sukces!');
                    window.location.replace(Routing.generate('institutions_index'));
                }
            })

            .fail(function () {
                toastr.error('Problem z połączeniem', 'Błąd!');
            });

    });

    $(document).on('click', 'button[name="delete-institution"]', function () {
        $.ajax({
            type: "POST",
            url: Routing.generate('institutions_delete'),
            data: {id: $(this).val()},
            dataType: "json"
        })
            .done(function (data) {
                if (data === true) {
                    toastr.success('Poradnia została usunięta', 'Sukces!');
                    window.location.replace(Routing.generate('institutions_index'));
                }
            })

            .fail(function () {
                toastr.error('Problem z połączeniem', 'Błąd!');
            });

    });

    $(document).on('click', '#update', function () {
        $.ajax({
            type: "POST",
            url: Routing.generate('institutions_update'),
            data: $('#update-form').serialize(),
            dataType: "json"
        })
            .done(function (data) {
                if (data === true) {
                    toastr.success('Poradnia została zaktualizowana', 'Sukces!');
                    window.location.replace(Routing.generate('institutions_index'));
                }
            })

            .fail(function () {
                toastr.error('Problem z połączeniem', 'Błąd!');
            });

    });


});