$(document).ready(function () {

    Vue.component('branch', {
        template: `
        <li id="diagnostic-forms">
             
             <span @click="toggleFolder" class="clickable"  v-if="model.children!=undefined"> <span :class=toggleButtonClass
                class="btn-sm btn-info" :title="toggleTitle"></span> </span>
                
             <span class="group"><b>{{model.name}}</b></span>
             
             <ul>
                <li v-for="form in model.forms"><a @click.prevent="route(form.formsId)" class="btn-sm btn-success clickable title" v-if="model.forms!=undefined"> wypełnij formularz </a><span class="title">{{ form.title }}</span> 
                </li>
            </ul>

            <ul v-if="visible">
                <branch class="item" v-for="formGroup, index in model.children" :model="formGroup" :key="formGroup.index">
                </branch>
            </ul>
        </li>
        `,
        props: ['model'],
        data: function () {
            return {
                visible: false
            }
        },
        computed: {
            toggleButtonClass:function () {
                return this.visible ? 'btn-sm btn-info glyphicon glyphicon-chevron-up' : 'btn-sm btn-info glyphicon glyphicon-chevron-down';
            },
            toggleTitle:function () {
                return this.visible ? 'Zwiń grupę' : 'Rozwiń grupę';
            }
        },

        methods: {
            toggleFolder: function () {
                this.visible = !this.visible;
            },
            route:function(formId){
                window.location.href =  Routing.generate('form',{'formId':formId});
            }
        }
    });

    var formsGroupsTree = new Vue({
        el: '#tree',
        data: {
            formsGroups: []
        },
        methods: {
            getFormGroupsData: function () {
                $.ajax({
                    url: Routing.generate('get_user_forms'),
                    success: function (data) {
                        formsGroupsTree.formsGroups = data;
                    }
                })
            }
        },
        mounted: function () {
            this.getFormGroupsData();
        },
        delimiters: ['@{', '}']
    });

});