var eventGlobal = new Vue();

var app = new Vue({
    el: '#root_app',
    data: {
        formsGroups: [],
        newGroup: {
            name:'',
            description:'',
            parentId :''
        },

        editGroup: {
            name:'',
            description:''
        },

        editMode: '',
        editButtonLabel: 'Zmień opis i nazwę'

    },

    methods: {
        editGroupButton: function(group) {
            this.editMode = this.editMode == group.id ? '' : group.id;
            this.editButtonLabel = this.editMode == group.id ? 'Anuluj edycję' : 'Zmień opis i nazwę' ;
            this.editGroup.name = group.name;
            this.editGroup.description = group.description;
        },

        updateGroup: function (id) {

            if(this.editGroup.name){
                this.editGroup.id=id;
                this.editButtonLabel = 'Zmień opis i nazwę';
                $.ajax({
                    type: 'POST',
                    data: app.editGroup,
                    url: Routing.generate('edit_forms_groups'),
                    success: function (data) {
                        if(data === false){
                            toastr.error('Nazwa grupy nie może być pusta');
                            return;
                        }
                        toastr.success('Grupa została zaktualizowana');
                        app.newGroup.description= '';
                        app.newGroup.name= '';
                        app.getGroupsData();
                        tree.getGroupsData();

                        app.editMode ='';
                    }
                })
            }
            else {
                toastr.error('Podaj nazwę grupy');
            }

        },

        
        addGroup: function () {
            this.newGroup.parentId = 0;
            this.newGroup.level = 0;
            if(this.newGroup.name) {
                $.ajax({
                    type: 'POST',
                    dataType: 'JSON',
                    data: app.newGroup,
                    url: Routing.generate('add_forms_groups'),
                    success: function (data) {
                        if(data === false){
                            toastr.error('Nazwa grupy nie może być pusta');
                            return;
                        }
                        toastr.success('Dodano grupę');
                        app.newGroup.description = '';
                        app.newGroup.name = '';
                        app.getGroupsData();
                        tree.getGroupsData();
                    }
                })
            }
            else {
                toastr.error('Podaj nazwę grupy');
            }
        },

        deleteGroup: function (id) {
            alertify.confirm('Czy na pewno chcesz usunąć grupę?', function () {
                $.ajax({
                    type: 'POST',
                    data: {id: id},
                    url: Routing.generate('delete_forms_groups'),
                    success: function (data) {
                        toastr.success(data);
                        app.newGroup.description= '';
                        app.newGroup.name= '';
                        app.getGroupsData();
                    }
                })
            }).set('labels', {ok: 'Tak',  cancel: 'Nie'}).setHeader('Usuwanie grupy');
        },

        formManagerUrl: function (id) {
            return Routing.generate('view_group_manager', { 'id' : id })
        },

        getGroupsData: function () {
            $.ajax({
                url: Routing.generate('get_forms_groups'),
                success: function (data) {
                    app.formsGroups = data;
                }
            })
        }
    },

    mounted: function () {
        this.getGroupsData();
        var that = this;
        eventGlobal.$on('refresh', function (data) {
            that.getGroupsData();
        })
    },
    delimiters: ['@{', '}']
});