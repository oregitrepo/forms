$(document).ready(function () {
    $(document).on('click', '#generate-submit', function () {
            $.ajax({
                type: "POST",
                url: Routing.generate('token_generate'),
                data: $('#generate-form').serialize()
            })
                .done(function (data) {
                    $("#token").val(data);
                })
                .fail(function () {
                    toastr.error('Problem z połączeniem', 'Błąd!');
                });
        }
    );

    $(document).on('click', '#save-submit', function () {
            $.ajax({
                type: "POST",
                url: Routing.generate('token_add'),
                data: $('#save-form').serialize()
            })
                .done(function (data) {
                    toastr.success(data, 'Sukces!');
                })
                .fail(function () {
                    toastr.error('Problem z połączeniem', 'Błąd!');
                });
        }
    );

    $('select').change(function () {
        var institutionId = $(this).val();
        var tokenId = $(this).data('tokenid');
        $.ajax({
            type: "POST",
            url: Routing.generate('token_assign'),
            data: {tokenId: tokenId, institutionId: institutionId}
        })
            .done(function (data) {
                toastr.success(data, 'Sukces!');
            })
            .fail(function () {
                toastr.error('Problem z połączeniem', 'Błąd!');
            });
    });
});