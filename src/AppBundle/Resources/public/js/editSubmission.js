$(document).ready(function () {

    var formId;

    $.ajax({
        type: "POST",
        url: Routing.generate('get_submission_form', {'submissionId': submissionId}),
        dataType: 'json'
    }).done(function (data) {


        var form = {};

        if (data.form.form.display === 'wizard') {
            console.log('wizard');
            form = new FormioWizard(document.getElementById('formio'), {
                noAlerts: true
            });
        }

        if (data.form.form.display === 'form') {
            console.log('form');
            form = new FormioForm(document.getElementById('formio'), {
                noAlerts: true,
            });
        }

        form.form = data.form.form;
        form.data = data.submission.submission;
        submissionId = data.submission.id;
        formId = data.form.id

        form.on('submit', (submission) => {

            submission.id = submissionId;
            submission.formId = formId;
            var submitUrl = Routing.generate('update_submission');

            $.ajax({
                type: "POST",
                data: submission,
                url: submitUrl,
                dataType: 'json'
            })
                .done(function (data) {
                    if(data === true) {
                        toastr.success('Wynik został edytowany');
                    }else{
                        toastr.error(data);
                    }
                })
                .fail(function () {
                    toastr.error('Wynik nie został edytowany');
                });

        });
        form.on('error', (errors) => {
            console.log('We have errors!');
        });

    });


});