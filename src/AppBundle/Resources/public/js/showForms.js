window.onload = function () {
    var renderUrl = Routing.generate('show_form_render',{'id': id });

    $.ajax({
        type: "POST",
        url: renderUrl,
        dataType: 'json'
    })
        .done(function (data) {
            var form= {};

            if(data.form.display === 'wizard') {
                console.log('wizard');
                form = new FormioWizard(document.getElementById('formio'), {
                    noAlerts: true,
                    readOnly: true
                });
            }

            if(data.form.display === 'form') {
                console.log('form');
                form = new FormioForm(document.getElementById('formio'), {
                    noAlerts: true,
                    readOnly: true
                });
            }
            var formId;

            var lengthElement = data.form.components.length;
            data.form.components[lengthElement - 1].theme = 'primary disabled';

            if (typeof data.form == 'object') {
                form.form = data.form;
            }
            else {
                form.form = $.parseJSON(data.form);
            }

            formId = data.id;
            form.on('submit', (submission) => {
                toastr.error('formularz tylko do wyświetlenia');
            });
        })
        .fail(function () {
            console.log('cos poszlo nie tak');
        });



}