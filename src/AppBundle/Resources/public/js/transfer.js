$(document).ready(function () {
    $(document).on('click', '#submit-transfer', function () {
        $.ajax({
            type: "POST",
            url: Routing.generate('cards_transfer_set'),
            data: {token: token, institution: $('#institution').find('option:selected').val()}
        })
            .done(function (data) {
                toastr.success('Token został przeniesiony', 'Sukces!');
                window.location.replace(Routing.generate('cards_index'));
            })
            .fail(function () {
                toastr.error('Problem z połączeniem', 'Błąd!');
            });
    });
});