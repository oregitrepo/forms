window.onload = function () {
    function getAllReports(token) {
        $.ajax({
            type: "POST",
            url: Routing.generate('reports_get_all'),
            data: {token: token},
            dataType: "json"
        })
            .done(function (data) {
                var results_body = $('#results-body');
                $.each(data, function (index, value) {
                    results_body.append('<tr><td>' + value.name + '</td><td>' + value[0].form.title + '</td><td>' + value.submitDate.date + '</td><td>' +
                        '<button class="btn-xs btn-primary" type="button" name="see-result" value="' + value.id + '">Obejrzyj</button>')
                });
            })
            .fail(function () {
                toastr.error('Problem z połączeniem', 'Błąd!');
            });
    }

    $(document).on('click', "button[name='see-result']", function () {
        var url = Routing.generate('report_get', {'id': $(this).val()});
        window.location.replace(url);
    });

    getAllReports($.cookie('token'));
};