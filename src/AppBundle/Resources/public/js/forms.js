toastr.options.positionClass = "toast-bottom-right";
var urlForms = Routing.generate('view_forms_render');

function addForm(data) {

        var tab = [];
        data.forms.forEach(function (element) {

            if(typeof element.form == 'object')
            {
                tab.push(element);
            }
            else
            {
                element.form = $.parseJSON(element.form);
                tab.push(element);
            }
        });

        app.forms = tab;
}

    $.ajax({
        method:"POST",
        url:urlForms,
        dataType:"json",
        success:addForm,
        error:function () {
            console.log('error');
        }
    });

var app = new Vue({
    delimiters: ['@{', '}'],
    el:'#root',
    data: {
        forms:''
    },
    methods:{
        statusForm:function (formId) {
            var urlStatus = Routing.generate('status_form');

            $.ajax({
                method:"POST",
                url:urlStatus,
                dataType:"json",
                data:{"formId":formId}
            })
                .done(function (data) {
                    addForm(data);
                    toastr.success('Status formularza został zmieniony');
                })
                .fail(function (data) {
                    $('#alert').html('<div class="alert alert-danger">Wystąpił błąd</div>');
                    toastr.error(data.responseText);
                });
        },
        rout:function (formId) {
            return Routing.generate('edit_form',{'id':formId });
        },
        showForm:function (formId) {
            return Routing.generate('show_form',{'id':formId });
        }

    }
});


