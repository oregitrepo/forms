$(document).ready(function () {

    $('form').submit(function (event) {
        event.preventDefault();

        var formGroupElements = app.formGroup.id;
        var formGroup = app.objectToArray(formGroupElements);

        if (formGroup[0]) {
            app.updateDataForm(id, $('#title').val(), formGroup, app.selected);
        }
        else{
            toastr.error('Nie wybrałeś żadnej grupy formularzy');
        }

    });

    Vue.component('branch',{
        template:`
            <li id="diagnostic-forms">
                <input v-if="model.children == undefined" type="checkbox" :id="model.id" @change="$emit('checked',check)" v-model="check[model.id]">
                
                <span @click="toggleFolder" class="clickable" v-if="model.children != undefined">
                    <span class="btn-sm btn-info" :class="toggleButtonClass" :title="toggleTitle"></span>
                </span>
                
                <span :for="model.name" class="group"><b>{{ model.name }}</b></span>

                <ul v-if="visible">
                    <branch class="item" v-for="group,index in model.children" :model="group" :index="index" :check="check" @checked="checked"></branch>
                </ul>
            </li>
        `,
        props:['model','check','index'],
        data:function () {
            return{
                visible:false
            };
        },
        computed:{
            toggleButtonClass:function () {
                return this.visible ? 'btn-sm btn-info glyphicon glyphicon-chevron-up' : 'btn-sm btn-info glyphicon glyphicon-chevron-down';
            },
            toggleTitle:function () {
                return this.visible ? 'Zwiń grupę' : 'Rozwiń grupę';
            }
        },
        methods:{
            toggleFolder:function () {
                this.visible = !this.visible;
            },
            checked:function (data) {
                this.$emit('checked',data);
            }
        }
    });


    var app = new Vue({
        delimiters: ['@{', '}'],
        el: '#root_app',
        data: {
            formsGroups: [],
            formGroup: {
                id: {}
            },
            reports:{},
            selected: report
},
    methods: {
        select:function (data) {
            this.formGroup.id = data;
        },
        updateDataForm: function (formId, title, formGroup, reportId) {
            var url = Routing.generate('edit_form_render');

            $.ajax({
                method: "POST",
                url: url,
                data: {
                    "formId": formId,
                    "title": title,
                    "formsGroupId": formGroup,
                    "selectedForm": formId,
                    "reportId": reportId
                },
                success:function (data) {
                    toastr.success(data);
                }
            });
        },
        getGroupsData: function () {
            var url = Routing.generate('get_user_forms');
            $.ajax({
                url: url,
                success: function (data) {
                    app.formsGroups = data;
                }
            });
        },
        getReportsData: function () {
            var url = Routing.generate('reports_get');
            $.ajax({
                method: "POST",
                url: url,
                success: function (data) {
                    app.reports = data;
                }
            });
        },
        selectedFormsGroups: function () {
            var editUrl = Routing.generate('get_forms_groups_form');

            $.ajax({
                method: "POST",
                url: editUrl,
                data: {'formId':id },
            success: function (data) {
                app.formGroup.id = this.arrayToObject(data);
            },
            error: function (data) {
            },
            arrayToObject:function (arr) {
                    var rv = {};
                    for (var i = 0; i < arr.length; ++i)
                        if (arr[i] !== undefined) {
                            rv[arr[i]] = true;
                        }
                    return rv;
            }
        });
        },
        objectToArray:function (formGroup) {
            return Object.keys(formGroup)
                .filter(function(k){return formGroup[k]})
                .map(Number);
        }
    },
    mounted: function () {
        this.getGroupsData();
        this.getReportsData();
        this.selectedFormsGroups();
    }
});
});