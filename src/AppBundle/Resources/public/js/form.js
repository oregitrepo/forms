$(document).ready( function () {
    var formId;

    $.ajax({
        type: "POST",
        url: Routing.generate('render_form'),
        dataType: 'json',
        data: {formId: formIdController}
    }).done(function (data) {

        var form = {};

        if (data.form.display === 'wizard') {
            console.log('wizard');
            form = new FormioWizard(document.getElementById('formio'), {
                noAlerts: true
            });
        }

        if (data.form.display === 'form') {
            console.log('form');
            form = new FormioForm(document.getElementById('formio'), {
                noAlerts: true,
            });
        }

        form.form = data.form;
        formId = data.id;

        form.on('submit', (submission) => {

            submission.token = $.cookie('token');
            submission.id = formId;
            var submitUrl = Routing.generate('submit_form');

            $.ajax({
                type: "POST",
                data: submission,
                url: submitUrl,
                dataType: 'json'
            })
                .done(function (data) {

                    if (data['result'] === true) {
                        toastr.success('Wynik został zapisany');
                    }
                    else {
                        var dataError = data['result'];
                        $('.error-field').remove();
                        for(obj in dataError){
                            var field = $('input[name="data['+obj+']"]');
                            $(dataError[obj]).each(function (ind,el) {
                                if(obj && field){
                                    field.closest('.form-group')
                                        .append('<p class="error-field">'+el+'</p>');
                                }
                            });
                        }
                    }
                })
                .fail(function (data) {
                    toastr.error('cos poszlo nie tak');
                });

        });
        form.on('error', (errors) => {
            console.log('We have errors!');
        });

    }).fail(function () {
        console.log('cos poszlo nie tak');
    });


});