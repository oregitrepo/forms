/**
 * Created by michal on 17.05.17.
 */
var application = new Vue({
    el: '#root_app',
    data: {
        userGroups: [],
        forms: [],
        allForms: [],
        allGroups: [],
        id: user_id,
        selectedGroup: '',
        selectedForm: '',
        groupName: group_name
    },

    methods: {
        getUserGroups: function () {
            $.ajax({
                type: 'POST',
                data: {id: this.id},
                url: Routing.generate('get_groups_to_forms_group'),
                success: function (data) {
                    application.userGroups = data.result;
                    application.allGroups = data.groups;
                }
            })
        },

        addGroup: function () {
            if(this.selectedGroup) {
                $.ajax({
                    type: 'POST',
                    data: {
                        formsGroupId: this.id,
                        selectedGroup: this.selectedGroup
                    },
                    url: Routing.generate('add_groups_to_forms_group'),
                    success: function (data) {
                        application.getUserGroups();
                        toastr.success(data);
                    }
                })
            }
        },

        deleteGroup: function (id) {
            var that = this;
            alertify.confirm('Czy na pewno chcesz usunąć grupę?', function () {
                $.ajax({
                    type: 'POST',
                    data: {
                        formsGroupId: that.id,
                        selectedGroup: id
                    },
                    url: Routing.generate('delete_groups_to_forms_group'),
                    success: function (data) {
                        application.getUserGroups();
                        toastr.success(data);
                    }
                })
            }).set('labels', {ok: 'Tak',  cancel: 'Nie'}).setHeader('Usuwanie grupy');
        }


    },

    mounted: function () {
        this.getUserGroups();
    },
    delimiters: ['@{', '}']
});