/**
 * Created by michal on 13.05.17.
 */
toastr.options.positionClass = "toast-bottom-right";

function showErrors(data) {
    $('.has-error').removeClass('has-error');
    $('.help-block').remove();

    for (var item in data) {
        $('#' + item).closest('.form-group').addClass('has-error');
        $('<span class="help-block" style="color:red">' + data[item] + '</span>').insertAfter('#' + item);
    }
}

Vue.component('edit-user', {
    template: `
         <div class="form-group"> 
                <div class="form-group">
                    <input class="form-control col-6" type="text" placeholder="Nowa nazwa użytkownika" v-model="datas.username" id="username">
                </div>
                <div class="form-group">
                    <input class="form-control col-6" type="email" placeholder="Nowy adres e-mail" v-model="datas.email" id="email">
                </div>
                <div class="form-group">
                    <input class="form-control col-6" type="password" placeholder="Nowe hasło" v-model="datas.password" id="password">
                </div>
                <div class="form-group">
                    <input class="form-control col-6" type="password" placeholder="Powtórz hasło" v-model="datas.repeat_password">
                </div>
                <div class="form-group">
                <label for="groupsCheckboxes">Wybierz grupy:</label>
                    <div id="groupsCheckboxes">
                        <label :for="key" class="checkbox-inline" v-for="group, key in groups">
                            <input type="checkbox" :id="key" :value="group" v-model="datas.groups">
                            <b v-text="group"></b>
                        </label>
                    </div>
                    </label>
                </div>
                 <button class="btn-xs btn-success" @click="editUser(id)">Zapisz zmiany</button>

        </div>
                   
               `,
    props: ['datas', 'groups', 'id'],
    data: function () {
        return {}
    },
    methods: {
        editUser: function (id) {
            this.datas.id = id;
            $.ajax({
                method: 'POST',
                url: Routing.generate('update_user'),
                dataType: 'json',
                data: this.datas,
                success: function (data) {
                    showErrors(data);
                    if (data === true) {
                        app.clearUserForm();
                        app.getUsersData();
                        app.editKey = '';
                        toastr.success('Zaktualizowano dane użytkownika');
                    }
                },
                error: function (data) {
                    toastr.error('Wystąpił błąd');
                }
            });

        }
    }
});

Vue.component('add-user', {
    template: `
         <div class="form-group"> 
             <div class="form-group">
                <input class="form-control " type="text" placeholder="Nazwa użytkownika" v-model="datas.username" id="username">
            </div>
             <div class="form-group">              
                <input class="form-control" type="email" placeholder="Adres e-mail" v-model="datas.email" id="email">
            </div>
            <div class="form-group">      
                <input class="form-control" type="password" placeholder="Hasło" v-model="datas.password" id="password">
            </div>
            <div class="form-group">   
                <input class="form-control" type="password" placeholder="Powtórz hasło" v-model="datas.repeat_password">
            </div>
            <div class="form-group">
            <label for="groupsCheckboxes">Wybierz grupy:</label>
                <div id="groupsCheckboxes" class="form-inline">
                    <label :for="key" class="checkbox-inline" v-for="group, key in groups">
                        <input type="checkbox" :id="key" :value="group" v-model="datas.groups">
                        <b v-text="group"></b>
                    </label>
                </div>
                </label>
            </div>
            <button class="btn-xs btn-success" @click="addUser">Zarejestruj</button>

        </div>
                   
               `,
    props: ['datas', 'groups'],
    methods: {
        addUser: function () {
            $.ajax({
                method: 'POST',
                url: Routing.generate('create_user'),
                dataType: 'json',
                data: this.datas,
                success: function (data) {
                    showErrors(data);
                    if(data === false){
                        toastr.error('Wszystkie pola są wymagane');
                    }
                    if (data === true) {
                        app.clearUserForm();
                        app.getUsersData();
                        app.newUser = false;
                        toastr.success('Dodano nowego użytkownika');
                    }

                },
                error: function (data) {
                    toastr.error('Wystąpił błąd');
                }
            });
        }

    }
});

var app = new Vue({
    el: '#root_app',
    data: {
        users: [],
        userData: {
            id: '',
            username: '',
            email: '',
            password: '',
            repeat_password: '',
            groups: []

        },
        groups: [],
        checkedGroups: [],
        editKey: '',
        newUser: false
    },

    methods: {

        editUserButton: function (id, groups) {
            app.clearUserForm();
            this.editKey = this.editKey === id ? '' : id;
            this.newUser = false;
            this.userData.groups = groups;
        },


        deleteUser: function (id) {
            alertify.confirm('Czy na pewno chcesz usunąc użytkownika?', function () {
                $.ajax({
                    method: 'POST',
                    url: Routing.generate('delete_user'),
                    dataType: 'json',
                    data: {'id': id},
                    success: function (data) {
                        app.getUsersData();
                        app.editKey = '';
                        app.newUser = false;
                        toastr.success('Usunięto użytkownika');
                    },
                    error: function (data) {
                        toastr.error(data.responseText);
                    }
                });
            }).set('labels', {ok: 'Tak', cancel: 'Nie'}).setHeader('Usuwanie użytkownika');

        },

        addUserButton: function () {
            this.newUser = !this.newUser;
            this.editKey = '';
            this.clearUserForm();
        },


        getUsersData: function () {
            $.ajax({
                url: Routing.generate('get_users'),
                success: function (data) {
                    app.users = data.users;
                    app.groups = data.groups;
                }
            })
        },

        clearUserForm: function () {
            app.userData = {
                id: '',
                username: '',
                email: '',
                password: '',
                repeat_password: '',
                groups: []
            };
        }
    },

    mounted: function () {
        this.getUsersData();
    },
    delimiters: ['@{', '}']
});