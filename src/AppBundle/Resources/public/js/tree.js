
Vue.component('branch', {
    template: `
        <li>
            <div class="form-inline">
                  <span>
                     
                     <span @click="deleteCascade" class="btn-sm btn-danger clickable button-fixed" v-if="isNotRoot" title="Usuń grupę">
                        <span class="glyphicon glyphicon-remove"></span>
                     </span> 
                     <span @click="toggleSaveEditMode" v-if="isNotRoot" class="clickable" :title="editGroupTitle"> 
                        <span :class="editSaveButtonClass"></span> 
                     </span>
                     <span @click="toggleFolder" class="clickable"  v-if="hasChildren" :title="toggleFolderButtonTitle"> 
                        <span :class="toggleFolderButtonClass"></span> 
                     </span>   
                     <span v-if="hasNoChildrenAndForms" @click="showSubGroup" class="clickable" title="Dodaj podgrupę">
                        <span class="btn-sm btn-info button-fixed">
                            <span class="glyphicon glyphicon-plus"></span>
                        </span>
                     </span>         
                     
                     <input v-if="editable" type="text" v-model="model.name" class="form-control" placeholder="Nazwa grupy*">
                     <input v-if="editable" type="text" v-model="model.description" class="form-control" placeholder="Opis grupy">
                     <span v-if="!editable"><b>{{model.name}}</b></span>
                     <a :href="formsGroupManagerUrl" v-if="!hasChildren" title="Formularze">
                        <span class="btn-sm btn-success clickable" >
                            <span class="glyphicon glyphicon-list-alt"></span>
                            <span class="forms-count" v-if="!hasNoForms">{{model.forms.length}}</span>
                        </span>
                     </a> 
                    
                 </span>
                 
                 
                <ul v-if="visible">
                    <li v-if="newGroupInputVisible" class="item">
                        <div class="form-inline">
                            <input type="text" v-model="newGroup" class="form-control input-add" placeholder="Dodaj nową grupę">
                            <span class="btn-sm btn-primary clickable button-fixed"  @click.prevent="addGroup" >
                                <span class="glyphicon glyphicon-plus "></span>
                            </span>
                        </div>
                    </li>
                    <branch class="item" v-for="item, index in model.children" :model="item" :index="index" :parent="model.children" :key="model.id">
                    </branch>
                </ul>
            </div>
             
            
        </li>
        `,
    props: ['model', 'index', 'parent'],
    data: function () {
        return {
            visible: false,
            editable: false,
            newGroup: '',
            isNewGroupVisible: false,
            previousName: this.model.name
        }
    },

    computed: {
        editSaveButtonClass: function () {
            return this.editable ? 'btn-sm btn-success  glyphicon glyphicon-ok-sign' : 'btn-sm btn-info glyphicon glyphicon-edit';
        },
        toggleFolderButtonClass: function () {
            return this.visible ? 'btn-sm btn-info glyphicon glyphicon-chevron-up' : 'btn-sm btn-info glyphicon glyphicon-chevron-down';
        },
        toggleFolderButtonTitle: function () {
            return this.visible ? "Zwiń grupę" : 'Rozwiń grupę';
        },
        editGroupTitle: function () {
            return this.editable ? ' Zapisz' : 'Edytuj nazwę grupy';
        },
        editButtonLabel: function () {
            return this.editable ? ' Zapisz ' : ' Edytuj ';
        },

        isNotRoot: function () {
            return this.index !== undefined;
        },

        hasChildren: function () {
            return this.model.children !== undefined && this.model.children.length > 0;
        },

        hasNoForms: function () {
            return this.model.forms === undefined || this.model.forms.length === 0;
        },

        formsGroupManagerUrl: function () {
            return Routing.generate('view_forms_manager', {'id': this.model.id})
        },

        hasNoChildrenAndForms: function () {
            return this.hasNoForms && this.isNotRoot && !this.hasChildren;
        },

        newGroupInputVisible: function () {
            return this.isNewGroupVisible || this.hasChildren || this.visible;
        }
    },

    methods: {
        toggleFolder: function () {
            this.visible = !this.visible;
        },

        showSubGroup: function () {
            this.isNewGroupVisible = !this.isNewGroupVisible;
            this.visible = !this.visible;
        },

        toggleSaveEditMode: function () {
            if (this.editable) {
                this.saveItem();
            }
            this.editable = !this.editable;
        },

        saveItem: function () {
            var elem = this;
            if (elem.model.name === '') {
                toastr.warning('Nazwa grupy nie może być pusta');
                elem.model.name = this.previousName;
                return;
            }

            $.ajax({
                type: 'POST',
                data: {name: elem.model.name, id: elem.model.id, description: elem.model.description},
                url: Routing.generate('edit_forms_groups'),
                success: function (data) {
                    toastr.success(data);
                    elem.model.name = '';
                    tree.getGroupsData();
                    app.getGroupsData();
                    eventGlobal.$emit('refresh');

                }
            })
        },


        addGroup: function () {
            var id = this.model.id, groupName, level;
            groupName = this.newGroup;
            level = this.model.level;
            var elem = this;
            $.ajax({
                type: 'POST',
                data: {name: groupName, description: '', parentId: id, level: level + 1},
                url: Routing.generate('add_forms_groups'),
                success: function (data) {
                    toastr.success(data);
                    elem.newGroup = '';
                    tree.getGroupsData();
                    eventGlobal.$emit('refresh')
                }
            })
        },
        deleteCascade: function () {
            var that = this;

            alertify.confirm('Czy na pewno chcesz usunąć grupę i jej podgrupy?', function () {
                $.ajax({
                    type: 'POST',
                    data: {tree: that.model},
                    url: Routing.generate('delete_forms_groups_cascade'),
                    success: function (data) {
                        toastr.success('Usunięto grupę i podgrupy');
                        tree.getGroupsData();
                        eventGlobal.$emit('refresh')
                    }
                })
            }).set('labels', {ok: 'Tak', cancel: 'Nie'}).setHeader('Usuwanie grupy');
        },

        deleteItem: function () {
            var id = this.model.id;
            alertify.confirm('Czy na pewno chcesz usunąć grupę?', function () {
                $.ajax({
                    type: 'POST',
                    data: {id: id},
                    url: Routing.generate('delete_forms_groups'),
                    success: function (data) {
                        toastr.success(data);
                        tree.getGroupsData();
                        eventGlobal.$emit('refresh')
                    }
                })
            }).set('labels', {ok: 'Tak', cancel: 'Nie'}).setHeader('Usuwanie grupy');
        }

    }
});

var tree = new Vue({
    el: '#tree',
    data: {
        tree: {
            name: 'Grupy',
            children: [],
            forms: [],
        }
    },
    methods: {
        getGroupsData: function () {
            $.ajax({
                url: Routing.generate('get_forms_groups_tree'),
                success: function (data) {
                    tree.tree.children = data;
                },
                cache: false
            })
        }
    },
    mounted: function () {
        this.getGroupsData();
    },
    delimiters: ['@{', '}']
});
