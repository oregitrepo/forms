window.onload = function () {
    function getAllGroups() {
        $.ajax({
            type: "POST",
            url: Routing.generate('groups_get_all'),
            dataType: "json"
        })
            .done(function (data) {
                var groups_body = $('#groups-body');
                groups_body.html('');
                console.log(data);
                $.each(data, function (index, value) {
                    groups_body.append('<tr><td>' + value.id + '</td><td>' + value.name + '</td><td>' + value.roles + '</td><td>' +
                        '<div role="group" class="btn-group"><button class="btn btn-xs btn-primary" type="button" name="edit-group" value="' + value.id + '">Edytuj</button>' +
                        '<button type="button" class="btn btn-xs btn-danger" name="delete-group" value="' + value.id + '">Usuń</button></div></td></tr>')
                });
            })
            .fail(function () {
                toastr.error('Problem z połączeniem', 'Błąd!');
            });
    }

    function getAllRoles() {
        $.ajax({
            type: "POST",
            url: Routing.generate('groups_roles_get_all'),
            dataType: "json"
        })
            .done(function (data) {
                var roles_body = $('#roles-body');
                $.each(data, function (index, value) {
                    if (!$('input[value="' + value + '"]').length) {
                        roles_body.append('<input type="checkbox" name="' + index + '" value="' + index + '">' + index + '<br>')
                    }


                });
            })
            .fail(function () {
                toastr.error('Problem z połączeniem', 'Błąd!');
            });
    }

    $(document).on('click', "button[name='edit-group']", function () {
        var url = Routing.generate('group_edit', {'id': $(this).val()});
        window.location.replace(url);
    });

    $(document).on('click', "button[name='delete-group']", function () {
            $.ajax({
                type: "POST",
                url: Routing.generate('group_delete', {'id': $(this).val()}),
                data: {id: 1}
            })
                .done(function (data) {
                    getAllGroups();
                    toastr.success(data, 'Sukces!');
                })
                .fail(function () {
                    toastr.error('Problem z połączeniem', 'Błąd!');
                });
        }
    );

    $(document).on('click', '#update-submit', function () {
            $.ajax({
                type: "POST",
                url: Routing.generate('group_update'),
                data: $('#update-form').serialize()
            })
                .done(function (data) {
                    toastr.success(data, 'Sukces!');
                    window.location.replace(Routing.generate('groups'));
                })
                .fail(function () {
                    toastr.error('Problem z połączeniem', 'Błąd!');
                });
        }
    );

    $(document).on('click', '#add-submit', function () {
            $.ajax({
                type: "POST",
                url: Routing.generate('group_create'),
                data: $('#add-form').serialize()
            })
                .done(function (data) {
                    toastr.success(data, 'Sukces!');
                    window.location.replace(Routing.generate('groups'));
                })
                .fail(function () {
                    toastr.error('Problem z połączeniem', 'Błąd!');
                });
        }
    );

    getAllGroups();
    getAllRoles();
};