$(document).ready(function () {

    function getUsers(id) {
        $.ajax({
            type: "POST",
            url: Routing.generate('cards_users_connections_get'),
            dataType: "json",
            data: {id: id, token: token}
        })
            .done(function (data) {
                var usersBody = $('#users-body');
                usersBody.html('');
                $.each(data.users, function (index, value) {
                    usersBody.append('<tr><td>' + value.username + '</td><td><input id="check' + value.id + '" name="user-check" type="checkbox" value="' + value.id + '" ></td></tr>')
                });
                $.each(data.connections, function (index, value) {
                    var checkbox = $('#check' + value.userId);
                    if (checkbox.length > 0) {
                        $(checkbox).prop('checked', true);
                    }
                });
            })
            .fail(function () {
                toastr.error('Problem z połączeniem', 'Błąd!');
            });
    }

    $('select').change(function () {
        getUsers($(this).val())
    });

    $(document).on('click', 'input[name="user-check"]', function () {
        if ($(this).is(":checked") === true) {
            $.ajax({
                type: "POST",
                url: Routing.generate('cards_connection_add'),
                dataType: "json",
                data: {user: $(this).val(), token: token}
            })
                .done(function (data) {
                    if (data === true) {
                        toastr.success('Sukces!', 'Przypisano token do użytkownika');
                    }
                })
                .fail(function () {
                    toastr.error('Problem z połączeniem', 'Błąd!');
                });
        } else if ($(this).is(":checked") === false) {
            $.ajax({
                type: "POST",
                url: Routing.generate('cards_connection_delete'),
                dataType: "json",
                data: {user: $(this).val(), token: token}
            })
                .done(function (data) {
                    if (data === true) {
                        toastr.success('Sukces!', 'Usunięto przypisanie');
                    }
                })
                .fail(function () {
                    toastr.error('Problem z połączeniem', 'Błąd!');
                });
        }
    });

    getUsers($('option:first').val());
});