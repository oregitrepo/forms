window.onload = function () {

    $('form').submit(function (event) {
        event.preventDefault();
        var formJson = $('textarea').val();

        if(formJson){
            try {
                formJson = $.parseJSON(formJson);
            }catch(data){
                document.getElementById('formio').innerHTML = '';
            }

            var form = {};

            if(formJson.display === 'wizard') {
                console.log('wizard');
                form = new FormioWizard(document.getElementById('formio'), {
                    noAlerts: true,
                    readOnly: true
                });
            }

            if(formJson.display === 'form') {
                console.log('form');
                form = new FormioForm(document.getElementById('formio'), {
                    noAlerts: true,
                    readOnly: true
                });
            }

            form.form = formJson;
        }else{
            toastr.error('Wypełnij pole');
            document.getElementById('formio').innerHTML = '';
        }
    });
}