/**
 * Created by michal on 17.05.17.
 */
var application = new Vue({
    el: '#root_app',
    data: {
        forms: [],
        allForms: [],
        id: user_id,
        selectedForm: '',
        groupName: group_name
    },

    methods: {

        getForms: function (id) {
            $.ajax({
                type: 'POST',
                data: {id: this.id},
                url: Routing.generate('get_forms_to_forms_group'),
                success: function (data) {
                    application.forms = data.result;
                    application.allForms = data.forms;
                }
            })
        },

        addForm: function () {
            if (this.selectedForm) {
                $.ajax({
                    type: 'POST',
                    data: {
                        formsGroupId: this.id,
                        selectedForm: this.selectedForm
                    },
                    url: Routing.generate('add_forms_to_forms_group'),
                    success: function (data) {
                        application.getForms();
                        toastr.success(data);
                    }
                })
            }


        },

        deleteForm: function (id) {
            var that = this;
            alertify.confirm('Czy na pewno chcesz usunąć formularz?', function () {
                $.ajax({
                    type: 'POST',
                    data: {
                        formsGroupId: that.id,
                        selectedForm: id
                    },
                    url: Routing.generate('delete_forms_to_forms_group'),
                    success: function (data) {
                        application.getForms();
                        toastr.success(data);
                    }
                })
            }).set('labels', {ok: 'Tak', cancel: 'Nie'}).setHeader('Usuwanie formularza');
        }

    },

    mounted: function () {
        this.getForms();
    },
    delimiters: ['@{', '}']
});