<?php

namespace AppBundle\Services\Validator;

use AppBundle\Services\Cmd\FormsCmdSrv;

class JsonValidator
{
    private $formsCmdSrv;

    function __construct(FormsCmdSrv $formsCmdSrv)
    {
        $this->formsCmdSrv = $formsCmdSrv;
    }

    public function isJsonValid($formJson)
    {
        json_decode($formJson);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function checkJson($formJson, $reportId)
    {
        if ($this->isJsonValid($formJson)) {
            $formJson = json_decode($formJson);
            $addNewForm = $this->formsCmdSrv->addNewForm($formJson, $reportId);
            return ['id' => $addNewForm,
                'statement' => true, 'form' => $formJson];
        } else {
            return ['statement' => false];
        }
    }
}