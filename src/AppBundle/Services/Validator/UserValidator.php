<?php


namespace AppBundle\Services\Validator;

use AppBundle\Constraints\EmailExists;
use AppBundle\Constraints\UsernameExists;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\IdenticalTo;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserValidator
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ValidateUser constructor.
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validatePasswords($pass1, $pass2, $min)
    {
        $error = $this->validator->validate($pass1, new IdenticalTo(['value' => $pass2, 'message' => "Passwords don't match."]));
        foreach ($error as $item) {
            return $item ? $item->getMessage() : null;
        }

        $error = $this->validator->validate($pass1,  new Length(['min' => $min]));
        foreach ($error as $item) {
            return $item ? $item->getMessage() : null;
        }
    }

    public function usernameExists($username)
    {
        $error = $this->validator->validate($username, new UsernameExists());
        foreach ($error as $item) {
            return $item ? $item->getMessage() : null;
        }
    }

    public function emailValid($email)
    {
        $error =  $this->validator->validate($email, new EmailExists());

        foreach ($error as $item) {
            return $item ? $item->getMessage() : null;
        }

        $error = $this->validator->validate($email, new Email());
        foreach ($error as $item) {
            return $item ? $item->getMessage() : null;
        }
    }
}