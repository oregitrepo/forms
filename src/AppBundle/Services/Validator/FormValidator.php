<?php

namespace AppBundle\Services\Validator;

use AppBundle\Services\Cmd\FormsCmdSrv;
use AppBundle\Services\Query\FormsQuerySrv;
use FormioValidator\Validator\FormValidator as FormioFormValidator;

class FormValidator
{
    private $formsQuerySrv;

    public function __construct(FormsQuerySrv $formsQuerySrv, FormsCmdSrv $formsCmdSrv)
    {
        $this->formsQuerySrv = $formsQuerySrv;
        $this->formsCmdSrv = $formsCmdSrv;
    }

    private function getFormById($id)
    {
        return $this->formsQuerySrv->getFormById($id);
    }

    public function validateSubmission($formId, $submission)
    {
        $form = $this->getFormById($formId)['form'];
        $result = (new FormioFormValidator())->validate($form, $submission);
        if ($result !== true) $result = $this->getErrorMessages($result);
        return $result;
    }

    private function getErrorMessages($fields)
    {
        $result = [];
        foreach ($fields as $errors) {
            foreach ($errors as $error) {
                $result[$error->getKey()][] = $error->getMessage();
            }
        }
        return $result;
    }
}