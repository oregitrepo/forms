<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 10.05.17
 * Time: 09:38
 */

namespace AppBundle\Services\Cmd;

use AppBundle\Repository\Cmd\SubmissionsCmdRep;
use AppBundle\Services\Validator\FormValidator;

class SubmissionsCmdSrv
{
    private $rep;
    private $formValidator;

    public function __construct(SubmissionsCmdRep $rep, FormValidator $formValidator)
    {
        $this->rep = $rep;
        $this->formValidator = $formValidator;
    }

    public function submitForm($token,$userId, $formId, $submission)
    {
        $isSubmissionValid = $this->formValidator->validateSubmission($formId, $submission);
        if ($isSubmissionValid === true) {
            $result = ($this->rep->submitForm($token,$userId, $formId, $submission))?true:'Wynik nie został zapisany';
        } else {
            $result = $isSubmissionValid;
        }
        return $result;
    }

    public function updateSubmission($formId,$submissionId,$updateData)
    {
        $isSubmissionValid = $this->formValidator->validateSubmission($formId, $updateData);
        if ($isSubmissionValid === true) {
            $result = ($this->rep->updateSubmission($submissionId, $updateData))?true:'Wynik nie został edytowany';
        } else {
            $result = 'Formularz nie jest poprawny';
        }
        return $result;
    }

    public function deleteSubmissionById($id)
    {
        return $this->rep->deleteSubmissionById($id);
    }

}