<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 17.05.17
 * Time: 12:00
 */

namespace AppBundle\Services\Cmd;


use AppBundle\Repository\Cmd\FormsToFormsGroupsCmdRep;
use AppBundle\Services\Query\FormsToFormsGroupsQuerySrv;

class FormsToFormsGroupsCmdSrv
{
    private $rep;
    private $srv;

    /**
     * FormsGroupsQuerySrv constructor.
     * @param $rep
     */
    public function __construct(FormsToFormsGroupsCmdRep $rep, FormsToFormsGroupsQuerySrv $srv)
    {
        $this->rep = $rep;
        $this->srv = $srv;
    }

    public function deleteForm($formId, $formsGroupId)
    {

        return $this->rep->deleteForm($formId, $formsGroupId);
    }

    public function deleteFormsFromFormsGroup($formsGroupId)
    {
        $result='';
        $forms = $this->srv->getFormsByFormsGroupId($formsGroupId);
        foreach ($forms as $form) {
            $result = $this->rep->deleteForm($form['formsId'], $formsGroupId);
        }
        return $result;
    }

    public function addForm($formId, $formsGroupId)
    {
        return $this->rep->addForm($formId, $formsGroupId);
    }

    public function addFormToGroups($formId,$formsGroupId)
    {
        return $this->rep->addFormToGroups($formId,$formsGroupId);
    }

    public function getFormsGroupsByFormId($formId)
    {
        return $this->rep->getFormsGroupsByFormId($formId);
    }

    public function updateFormsGroupsToForm($formId,$formsGroupId)
    {
        return $this->rep->updateFormsGroupsToForm($formId,$formsGroupId);
    }
}