<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 10.05.17
 * Time: 09:37
 */

namespace AppBundle\Services\Cmd;

use AppBundle\Services\Validator\UserValidator;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\GroupManager;

class UsersCmdSrv
{
    private $userManager;
    private $groupManager;
    private $em;
    private $userValidator;

    public function __construct(UserManager $userManager, GroupManager $groupManager, EntityManager $em, UserValidator $userValidator)
    {
        $this->userManager = $userManager;
        $this->groupManager = $groupManager;
        $this->em = $em;
        $this->userValidator = $userValidator;
    }

    public function createUser($userData)
    {
        if ($errors = $this->validateUser($userData)) {
            return $errors;
        }

        $user = $this->userManager->createUser();
        $this->addUserToGroups($user, isset($userData['groups']) ? $userData['groups'] : []);

        $errors[] = $this->setField($user, $userData['username'], 'Username', true);
        $errors[] = $this->setField($user, $userData['email'], 'Email', true);
        $errors[] = $this->setField($user, $userData['password'], 'PlainPassword', true);

        if($this->removeEmptyErrors($errors)) {
            return false;
        };

        $user->setEnabled(true);
        $this->em->persist($user);
        $this->em->flush();
        return true;
    }

    public function updateUser($userData)
    {
        if ($errors = $this->validateUser($userData)) {
            return $errors;
        }

        $user = $this->userManager->findUserBy(array("id" => $userData["id"]));
        if ($user == null) {
            return null;
        }

        $this->addUserToGroups($user, isset($userData['groups']) ? $userData['groups'] : []);
        $this->setField($user, $userData['username'], 'Username', false);
        $this->setField($user, $userData['email'], 'Email', false);
        $this->setField($user, $userData['password'], 'PlainPassword', false);

        $user->setEnabled(true);
        $this->userManager->updateUser($user);
        $this->em->flush();

        return true;
    }

    public function deleteUser($userId)
    {
        $user = $this->userManager->findUserBy(array("id" => $userId));
        $this->em->remove($user);
        $this->em->flush();
        return true;
    }

    public function addUserToGroups($user, $groupsNames)
    {
        if (empty($groupsNames)) {
            $user = $this->removeGroupsFromUser($user);
            return $user;
        }

        $user = $this->removeGroupsFromUser($user);
        foreach ($groupsNames as $groupName) {
            $group = $this->groupManager->findGroupByName($groupName);
            $user->addGroup($group);
        }
        return $user;
    }

    private function validateUser($userData)
    {
        $errorMessages['password'] = $this->userValidator->validatePasswords($userData['password'], $userData['repeat_password'], 6);
        $errorMessages['email'] = $this->userValidator->emailValid($userData['email']);
        $errorMessages['username'] = $this->userValidator->usernameExists($userData['username']);

        return $this->removeEmptyErrors($errorMessages);
    }

    private function removeGroupsFromUser($user)
    {
        foreach ($user->getGroupNames() as $groupName) {
            $group = $this->groupManager->findGroupByName($groupName);
            $user->removeGroup($group);
        }
        return $user;
    }

    private function setField($user, $field, $fieldName, $isMandatory)
    {
        if ($isMandatory && $field == '') {
            return true;
        }
        $fieldName = 'set' . $fieldName;
        if ($field != '') {
            $user->$fieldName($field);
        }
    }

    private function removeEmptyErrors($errorMessages): array
    {
        return array_filter($errorMessages, function ($error) {
            return $error;
        });
    }

}