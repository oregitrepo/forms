<?php

namespace AppBundle\Services\Cmd;

use AppBundle\Repository\Cmd\TokensCmdRep;
use AppBundle\Services\Query\TokensQuerySrv;

class TokensCmdSrv
{
    private $rep;
    private $tokensQuerySrv;

    public function __construct(TokensCmdRep $rep, TokensQuerySrv $tokensQuerySrv)
    {
        $this->rep = $rep;
        $this->tokensQuerySrv = $tokensQuerySrv;
    }

    /**
     * @param $token
     * @return string
     */
    public function addNewToken($token, $genderId, $provinceId, $populationId, $fatherEducationId, $motherEducationId)
    {
        $result = 'Token nie jest unikalny';
        if ($this->isTokenUnique($token) === true) {
            $result = $this->rep->addNewToken($token, $genderId, $provinceId, $populationId, $fatherEducationId, $motherEducationId);
        }
        return $result;
    }

    private function isTokenUnique($token)
    {
        return (empty($this->tokensQuerySrv->getTokenByToken($token)));
    }

    public function assignTokenToInstitution($tokenId, $institutionId)
    {
        return $this->rep->assignTokenToInstitution($tokenId, $institutionId);
    }
}