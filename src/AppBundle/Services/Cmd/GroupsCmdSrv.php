<?php

namespace AppBundle\Services\Cmd;

use AppBundle\Repository\Cmd\GroupsCmdRep;

class GroupsCmdSrv
{
    private $rep;

    public function __construct(GroupsCmdRep $rep)
    {
        $this->rep = $rep;
    }

    public function createGroup($name, $roles)
    {
        return $this->rep->createGroup($name, $roles);
    }

    public function updateGroup($id, $name, $roles)
    {
        return $this->rep->updateGroup($id, $name, $roles);
    }

    public function deleteGroup($id)
    {
        return $this->rep->deleteGroup($id);
    }
}