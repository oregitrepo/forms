<?php

namespace AppBundle\Services\Cmd;

use AppBundle\Repository\Cmd\TokensUsersCmdRep;
use AppBundle\Services\Query\TokensUsersQuerySrv;

class TokensUsersCmdSrv
{

    private $rep;
    private $querySrv;

    public function __construct(TokensUsersCmdRep $rep, TokensUsersQuerySrv $querySrv)
    {
        $this->rep = $rep;
        $this->querySrv = $querySrv;
    }

    public function addConnection($userId, $tokenId, $institutionId)
    {
        return $this->rep->addConnection($tokenId, $userId, $institutionId);
    }

    public function deleteConnection($userId, $tokenId, $institutionId)
    {
        return $this->rep->deleteConnection($tokenId, $userId, $institutionId);
    }
}