<?php

namespace AppBundle\Services\Cmd;

use AppBundle\Repository\Cmd\FormsCmdRep;

class FormsCmdSrv
{
    private $rep;

    /**
     * FormsCmdSrv constructor.
     * @param FormsCmdRep $rep
     */
    public function __construct(FormsCmdRep $rep)
    {
        $this->rep = $rep;
    }

    public function addNewForm($form,$reportId)
    {
        return $this->rep->addNewForm($form,$reportId);
    }

    public function setStatus($formId)
    {
        return $this->rep->setStatus($formId);
    }

    public function setTitle($formId,$title)
    {
        return $this->rep->setTitle($formId,$title);
    }

    public function setReport($formId,$reportId)
    {
        return $this->rep->setReport($formId,$reportId);
    }
}
