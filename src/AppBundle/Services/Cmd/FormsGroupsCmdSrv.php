<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 17.05.17
 * Time: 12:00
 */

namespace AppBundle\Services\Cmd;

use AppBundle\Repository\Cmd\FormsGroupsCmdRep;

class FormsGroupsCmdSrv
{
    private $rep;
    private $srv;

    /**
     * FormsGroupsQuerySrv constructor.
     * @param $rep
     */
    public function __construct(FormsGroupsCmdRep $rep, FormsToFormsGroupsCmdSrv $srv)
    {
        $this->rep = $rep;
        $this->srv = $srv;
    }

    public function editFormsGroup($id, $name, $description)
    {
        return $name ? $this->rep->editFormsGroup($id, $name, $description) : false;
    }

    public function deleteFormsGroupById($id)
    {
        $this->srv->deleteFormsFromFormsGroup($id);
        return $this->rep->deleteFormsGroupById($id);
    }


    public function deleteFormsGroupCascade($formsGroup)
    {
        if (isset($formsGroup['children'])) {
            foreach ($formsGroup['children'] as $child) {
                $this->deleteFormsGroupCascade($child);
            }
        }
        $this->deleteFormsGroupById($formsGroup['id']);
        return true;
    }

    public function addFormsGroup($name, $description, $parentId, $level)
    {
        if($parentId == null) {
            $parentId = 0;
        }
        if($level == null) {
            $level = 0;
        }
        return $name ? $this->rep->addFormsGroup($name, $description, $parentId, $level) : false;
    }
}