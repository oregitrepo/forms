<?php

namespace AppBundle\Services\Cmd;


use AppBundle\Repository\Cmd\InstitutionsCmdRep;

class InstitutionsCmdSrv
{
    private $rep;

    public function __construct(InstitutionsCmdRep $rep)
    {
        $this->rep = $rep;
    }

    public function addInstitution($name, $description, $userId)
    {
        return $this->rep->addInstitution($name, $description, $userId);
    }

    public function updateInstitution($id, $name, $description, $userId)
    {
        return $this->rep->updateInstitution($id, $name, $description, $userId);
    }

    public function deleteInstitutionById($id)
    {
        return $this->rep->deleteInstitutionById($id);
    }


}