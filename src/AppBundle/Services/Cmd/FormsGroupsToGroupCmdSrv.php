<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 17.05.17
 * Time: 12:00
 */

namespace AppBundle\Services\Cmd;


use AppBundle\Repository\Cmd\FormsGroupsToGroupCmdRep;

class FormsGroupsToGroupCmdSrv
{
    private $rep;

    /**
     * FormsGroupsQuerySrv constructor.
     * @param $rep
     */
    public function __construct(FormsGroupsToGroupCmdRep $rep)
    {
        $this->rep = $rep;
    }

    public function deleteGroup($groupId, $formsGroupId)
    {
        return $this->rep->deleteGroup($groupId, $formsGroupId);
    }

    public function addGroup($groupId, $formsGroupId)
    {
        return $this->rep->addGroup($groupId, $formsGroupId);
    }
}