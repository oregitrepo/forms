<?php

namespace AppBundle\Services;

use AppBundle\Services\Query\TokensQuerySrv;

class TokenGeneratorSrv
{
    private $tokensQuerySrv;

    public function __construct(TokensQuerySrv $tokensQuerySrv)
    {
        $this->tokensQuerySrv = $tokensQuerySrv;
    }

    public function getToken($name, $surname, $pesel)
    {
        $token = $this->generateToken($name, $surname, $pesel);
        while ($this->isTokenUnique($token) === false) {
            $token = $this->generateToken($name, $surname, $pesel);
        }
        return $token;
    }

    private function generateToken($name, $surname, $pesel)
    {
        $dataTable = str_split($name . $surname . $pesel);
        shuffle($dataTable);
        $data = implode($dataTable);
        return hash('adler32', $data);
    }

    private function isTokenUnique($token)
    {
        return (empty($this->tokensQuerySrv->getTokenByToken($token)));
    }

}