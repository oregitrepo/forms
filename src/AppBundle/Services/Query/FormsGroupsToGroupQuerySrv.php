<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 17.05.17
 * Time: 11:59
 */

namespace AppBundle\Services\Query;


use AppBundle\Repository\Query\FormsGroupsToGroupQueryRep;

class FormsGroupsToGroupQuerySrv
{
    private $rep;
    private $groupsQuerySrv;
    /**
     * FormsGroupsQuerySrv constructor.
     * @param $rep
     */
    public function __construct(FormsGroupsToGroupQueryRep $rep, GroupsQuerySrv $groupsQuerySrv)
    {
        $this->rep = $rep;
        $this->groupsQuerySrv = $groupsQuerySrv;
    }


    public function getGroupsByFormsGroupId($id)
    {
        $groups = $this->rep->getGroupsByFormsGroupId($id);

        foreach ($groups as &$group) {
            $name = $this->groupsQuerySrv->getGroupById($group['groupId'])['name'];
            $group['groupName'] = $name;

        }
        return $groups;
    }
}