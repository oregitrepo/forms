<?php

namespace AppBundle\Services\Query;

use AppBundle\Repository\Query\TokensQueryRep;

class TokensQuerySrv
{
    private $rep;
    private $institutions;

    /**
     * SubmissionsQuerySrv constructor.
     * @param TokensQueryRep $rep
     * @param InstitutionsQuerySrv $institutions
     */
    public function __construct(TokensQueryRep $rep, InstitutionsQuerySrv $institutions)
    {
        $this->rep = $rep;
        $this->institutions = $institutions;
    }

    /**
     * @param $token
     * @return array
     */
    public function getTokenByToken($token)
    {
        return $this->rep->getTokenByToken($token);
    }

    public function getAllTokens()
    {
        return $this->rep->getAllTokens();
    }

    public function getTokenAssignedToUser($token, $userId)
    {
        $institution = $this->institutions->getInstitutionByToken($token);
        return $this->rep->getTokenAssignedToUser($token, $userId, $institution['id']);
    }

    public function doesTokenExists($token, $userId)
    {
        return (empty($this->getTokenAssignedToUser($token, $userId))) ? false : true;
    }

    public function getTokensByUserId($userId)
    {
        return $this->rep->getTokensByUserId($userId);
    }

    public function getTokenById($id)
    {
        return $this->rep->getTokenById($id);
    }
}