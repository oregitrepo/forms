<?php

namespace AppBundle\Services\Query;

use AppBundle\Repository\Query\TokensUsersQueryRep;

class TokensUsersQuerySrv
{

    private $rep;

    public function __construct(TokensUsersQueryRep $rep)
    {
        $this->rep = $rep;
    }

    public function getConnectionsByTokenId($tokenId)
    {
        return $this->rep->getConnectionsByTokenId($tokenId);
    }
}