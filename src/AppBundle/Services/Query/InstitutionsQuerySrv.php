<?php

namespace AppBundle\Services\Query;

use AppBundle\Repository\Query\InstitutionsQueryRep;

class InstitutionsQuerySrv
{
    private $rep;

    public function __construct(InstitutionsQueryRep $rep)
    {
        $this->rep = $rep;
    }

    public function getAllInstitutions()
    {
        return $this->rep->getAllInstitutions();
    }

    public function getInstitutionById($id)
    {
        return $this->rep->getInstitutionById($id);
    }

    public function getInstitutionByToken($token)
    {
        return $this->rep->getInstitutionByToken($token);
    }
}