<?php

namespace AppBundle\Services\Query;

use AppBundle\Repository\Query\ReportsDataQueryRep;

class ReportsDataQuerySrv
{
    private $rep;

    /**
     * SubmissionsQuerySrv constructor.
     * @param $rep
     */
    public function __construct(ReportsDataQueryRep $rep)
    {
        $this->rep = $rep;
    }

    public function getAllReports($token)
    {
        return $this->rep->getAllReports($token);
    }

    public function getReportResult($submissionId)
    {
        $data = $this->rep->getReportData($submissionId);
        $className = vsprintf(<<<PATH
\AppBundle\Reports\%s
PATH
            , [$data->getClass()]);
        return (new $className($data))->getResult();
    }

    public function getReports()
    {
        return $this->rep->getReports();
    }
}