<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 10.05.17
 * Time: 09:37
 */

namespace AppBundle\Services\Query;

use AppBundle\Repository\Query\UsersQueryRep;
use FOS\UserBundle\Doctrine\UserManager;

class UsersQuerySrv
{
    private $rep;
    private $userManager;

    public function __construct(UsersQueryRep $rep, UserManager $userManager)
    {
        $this->rep = $rep;
        $this->userManager = $userManager;
    }

    public function getAllUsers()
    {
        $usersData = [];
        $users = $this->userManager->findUsers();

        foreach ($users as $user) {
            $usersData[] = [
                'id' => $user->getId(),
                'username' => $user->getUsername(),
                'email' => $user->getEmail(),
                'last_login' => ($user->getLastLogin()) ? $user->getLastLogin()->format('Y-m-d H:i:s') : $user->getLastLogin(),
                'groups_values' => $user->getGroupNames(),
                'groups' => implode(', ', $user->getGroupNames())
            ];
        }

        usort($usersData, function ($a, $b) {
            return $a['username'] <=> $b['username'];
        });
        return $usersData;
    }

    public function getUsersByGroupName($groupName)
    {
        return $this->rep->getUsersByGroupName($groupName);
    }

    public function getUsersByGroupId($groupId)
    {
        return $this->rep->getUsersByGroupId($groupId);
    }
}