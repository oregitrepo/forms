<?php

namespace AppBundle\Services\Query;

class StatsQuerySrv
{
    private $subSrv;
    private $formsSrv;
    private $submissions;

    public function __construct(SubmissionsQuerySrv $subSrv, FormsQuerySrv $formsSrv)
    {
        $this->subSrv = $subSrv;
        $this->formsSrv = $formsSrv;
    }

    public function getFormsWithSubmissions()
    {
        $forms = $this->formsSrv->getFormsWithSubmissions();
        foreach ($forms as &$form) {
            if(is_string($form['form'])) {
                    $form['form'] = json_decode($form['form'],true);
            }
        }
        return $forms;
    }

    public function getFormStats($id)
    {
        $form = $this->formsSrv->getFormById($id);
        $this->submissions = $this->subSrv->getSubmissionsByFormId($id);
        $result = [];
        foreach ($form['form']['components'] as $field) {
            if (array_key_exists('questions', $field)) {
                foreach ($field['questions'] as $question) {
                    $answers = $this->getFieldAnswers($field, $question['label'], $question['value'], true);
                    (!empty($answers)) ? $result = $result + $answers : false;
                }
            } else {
                $answers = $this->getFieldAnswers($field, $field['label'], $field['key']);
                (!empty($answers)) ? $result = $result + $answers : false;
            }
        }
        return $result;
    }

    private function getFieldAnswers($field, $question, $keyName, $survey = false)
    {
        $answers = [];
        foreach ($this->submissions as $submission) {
            foreach ($submission['submission'] as $answerKey => $answer) {
                if ($survey && $field['key'] == $answerKey) {
                    foreach ($answer as $value) {
                        $answers = $this->getAnswerForField($value, $answers);
                    }
                } else if ($field['key'] == $answerKey) {
                    $answers = $this->getAnswerForField($answer, $answers);
                }
            }
        }
        if (!empty($answers)) {
            $result[$keyName]['question'] = $question;
            arsort($answers, SORT_NUMERIC);
            $result[$keyName]['answers'] = $answers;
        } else {
            $result = [];
        }
        return $result;
    }

    private function getAnswerForField($answer, $result)
    {
        if (array_key_exists($answer, $result) == true) {
            $result[$answer]++;
        } else {
            $result[$answer] = 1;
        }
        return $result;
    }
}