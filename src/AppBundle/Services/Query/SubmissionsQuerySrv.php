<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 10.05.17
 * Time: 09:37
 */

namespace AppBundle\Services\Query;

use AppBundle\Repository\Query\SubmissionsQueryRep;

class SubmissionsQuerySrv
{
    private $rep;
    private $formsQuerySrv;

    /**
     * SubmissionsQuerySrv constructor.
     * @param SubmissionsQueryRep $rep
     * @param FormsQuerySrv $formsQuerySrv
     */
    public function __construct(SubmissionsQueryRep $rep, FormsQuerySrv $formsQuerySrv)
    {
        $this->rep = $rep;
        $this->formsQuerySrv = $formsQuerySrv;
    }

    public function getAllSubmissions()
    {

        return $this->rep->getAllSubmissions();
    }

    public function getSubmissionById($id)
    {
        return $this->rep->getSubmissionById($id);
    }

    public function getSubmissionsByFormId($formId)
    {
        return $this->rep->getSubmissionByFormId($formId);
    }

    public function getFormsWithSubmissionsByTokenAndUserId($token, $userId)
    {
        $submissions = $this->rep->getSubmissionsByTokenAndUserId($token, $userId);

        foreach ($submissions as &$submission) {
            $form = $this->formsQuerySrv->getFormById($submission['formId']);

            if (!$form) {
                return 'Brak formularza o ID: ' . $submission['formId'];
            }

            $submission['formTitle'] =  $form['form']['title'];
            $submission['labels'] = $this->extractLabels($form);
         }

        return $submissions;
    }

    public function extractLabels($form)
    {
        $labels = [];
        $temp = [];
        $this->extractAllComponents($form['form'], $labels);
        foreach ($labels as $label) {
            $temp[$label['key']]['name'] = $label['label'];
            $temp[$label['key']]['questions'] = $label['questions'];
            $temp[$label['key']]['values'] = $label['values'];
            $temp[$label['key']]['type'] = $label['type'];
        }
        return $temp;
    }

    public function extractAllComponents($element, &$tab)
    {
        if (isset($element['components'])) {
            foreach ($element['components'] as $component) {
                $item = $this->extractAllComponents($component, $tab);
                if($item){
                    $tab[] = $item;
                }
            }
        } else {
            return [
                'values' => isset($element['values']) ? $this->generateValuesLabelArray($element['values']) : null,
                'questions' => isset($element['questions']) ? $this->generateValuesLabelArray($element['questions']) : null,
                'key' => $element['key'],
                'label' => isset($element['label']) ? $element['label'] : null,
                'type' => $element['type']
            ];
        }
    }

    private function generateValuesLabelArray($values)
    {
        $data = [];
        foreach ($values as $value) {
            $data[$value['value']] = $value['label'];
        }
        return $data;
    }
}