<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 17.05.17
 * Time: 11:59
 */

namespace AppBundle\Services\Query;


use AppBundle\Repository\Query\FormsToFormsGroupsQueryRep;

class FormsToFormsGroupsQuerySrv
{
    private $rep;
    private $formsQuerySrv;


    /**
     * FormsGroupsQuerySrv constructor.
     * @param $rep
     */
    public function __construct(FormsToFormsGroupsQueryRep $rep, FormsQuerySrv $formsQuerySrv)
    {
        $this->rep = $rep;
        $this->formsQuerySrv = $formsQuerySrv;
    }

    public function getFormsByFormsGroupId($id)
    {
        $forms = $this->rep->getFormsByFormsGroupId($id);
        foreach ($forms as $key => &$form) {
            $formData = $this->formsQuerySrv->getFormById($form['formsId']);
            if (!empty($formData)) {
                if ($formData['status'] == false) {
                    unset($forms[$key]);
                    continue;
                }

                $formData = $formData['form'];

                if (is_string($formData)) {
                    $title = json_decode($formData, true)['title'];
                    $form['title'] = $title;
                } else {
                    $title = $formData['title'];
                    $form['title'] = $title;

                }
            }
            else {
                unset($forms[$key]);
            }
        }
        return $forms;
    }
}