<?php

namespace AppBundle\Services\Query;

use AppBundle\Repository\Query\GroupsQueryRep;

class GroupsQuerySrv
{
    private $rep;

    public function __construct(GroupsQueryRep $rep)
    {
        $this->rep = $rep;
    }

    public function getAllGroups()
    {
        return $this->rep->getAllGroups();
    }

    public function getGroupById($id)
    {
        return $this->rep->getGroupById($id);
    }
}