<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 17.05.17
 * Time: 11:58
 */

namespace AppBundle\Services\Query;


use AppBundle\Repository\Query\FormsGroupsQueryRep;

class FormsGroupsQuerySrv
{
    private $rep;
    private $srv;

    /**
     * FormsGroupsQuerySrv constructor.
     * @param $rep
     */
    public function __construct(FormsGroupsQueryRep $rep, FormsToFormsGroupsQuerySrv $srv)
    {
        $this->rep = $rep;
        $this->srv = $srv;
    }

    public function getAllFormsGroups()
    {
        return $this->rep->getAllFormsGroups();
    }

    public function getFormsGroupsByParentId($id)
    {
        return $this->rep->getFormsGroupsByParentId($id);
    }

    public function getFormsGroupById($id)
    {
        return $this->rep->getFormsGroupById($id);
    }

    public function getFormsGroupsTree()
    {
        $tab = $this->rep->getAllFormsGroups();
        $time_start = microtime(true);
        $tree = $this->buildTree($tab);
        $time_end = microtime(true);
        $execution_time_recursive = ($time_end - $time_start);

//        $time_start = microtime(true);
//        $tree2 = $this->buildLeveledTree($tab);
//        $time_end = microtime(true);
//        $execution_time_non_recursive = ($time_end - $time_start);

        return $tree;

    }


    public function buildLeveledTree(array $elements)
    {
        $tree = [];
        foreach ($elements as $key => $element) {
            $element['forms'] = $this->srv->getFormsByFormsGroupId($element['id']);
            $tree[$element['level']][$element['id']] = $element;
        }
        $lvl = sizeof($tree) - 1;

        for ($index = $lvl; $index > 0; $index--) {
            foreach ($tree[$index] as $item) {
                $tree[$index - 1][$item['parentId']]['children'][] = $item;
                unset($tree[$index]);
            }
        }

        return $tree;
    }

    public function buildTree(array $elements, $parentId = null)
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parentId'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                } else {
                    $element['forms'] = $this->srv->getFormsByFormsGroupId($element['id']);
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }
}