<?php

namespace AppBundle\Services\Query;

use AppBundle\Repository\Query\DictQueryRep;

class DictQuerySrv
{
    private $rep;

    public function __construct(DictQueryRep $rep)
    {
        $this->rep = $rep;
    }

    public function getDictValuesByDictSlag($slag)
    {
        return $this->rep->getDictValuesByDictSlag($slag);
    }

    public function getAllDictsWithValues()
    {
        $result = [];
        foreach ($this->rep->getAllDicts() as $dict) {
            $result[$dict['slag']] = $dict;
            $result[$dict['slag']]['values'] = $this->getDictValuesByDictSlag($dict['slag']);
        }
        return $result;
    }
}