<?php
/**
 * Created by PhpStorm.
 * User: Martino
 * Date: 20.05.2017
 * Time: 13:27
 */

namespace AppBundle\Services\Query;

use AppBundle\Repository\Query\UserFormsQueryRep;

class UserFormsQuerySrv
{
    private $UserFormsRep;
    private $formsToFormsGroupsSrv;
    private $formsGroupsSrv;

    public function __construct(UserFormsQueryRep $UserFormsRep,FormsToFormsGroupsQuerySrv $formsToFormsGroupsSrv,FormsGroupsQuerySrv $formsGroupsSrv)
    {
        $this->UserFormsRep = $UserFormsRep;
        $this->formsToFormsGroupsSrv = $formsToFormsGroupsSrv;
        $this->formsGroupsSrv = $formsGroupsSrv;
    }

    public function getFormsGroupsByUserId($userId)
    {
        $allFormsGroups = $this->formsGroupsSrv->getAllFormsGroups();
        $formsGroupsTree = $this->buildTree($allFormsGroups);
        $formsGroupsToUser = $this->UserFormsRep->getFormsGroupsByUserId($userId);
        $filteredFormsGroups = array_filter($formsGroupsTree,function($formGroupTree) use($formsGroupsToUser){
            $bool = false;
            foreach ($formsGroupsToUser as $key => $formGroupToUser){
                if($formGroupTree['id'] == $formGroupToUser['id']){
                    $bool = true;
                }
            }
            return $bool;
        });

        return $filteredFormsGroups;
    }

    public function buildTree(array $elements, $parentId = null) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parentId'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                else{
                    $element['forms'] = $this->formsToFormsGroupsSrv->getFormsByFormsGroupId($element['id']);
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }
}