<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 10.05.17
 * Time: 09:36
 */

namespace AppBundle\Services\Query;

use AppBundle\Repository\Query\FormsQueryRep;

class FormsQuerySrv
{
    private $rep;

    /**
     * FormsQuerySrv constructor.
     * @param $rep
     */
    public function __construct(FormsQueryRep $rep)
    {
        $this->rep = $rep;
    }

    public function getFormById($id)
    {
        return $this->rep->getFormById($id);
    }

    public function getFormsWithSubmissions()
    {
        return $this->rep->getFormsWithSubmissions();
    }

    public function getAllForms()
    {
        return $this->rep->getAllForms();
    }

    public function getAllActiveForms()
    {
        return $this->rep->getAllActiveForms();
    }


}