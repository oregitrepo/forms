<?php

namespace AppBundle\Entity;

/**
 * Submissions
 */
class Submissions
{
    /**
     * @var string
     */
    private $token;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $formId;

    /**
     * @var array
     */
    private $submission;

    /**
     * @var \DateTime
     */
    private $submitDate;

    /**
     * @var integer
     */
    private $id;

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set UserId
     *
     * @param string $userId
     *
     * @return Submissions
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Submissions
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set formId
     *
     * @param integer $formId
     *
     * @return Submissions
     */
    public function setFormId($formId)
    {
        $this->formId = $formId;

        return $this;
    }

    /**
     * Get formId
     *
     * @return integer
     */
    public function getFormId()
    {
        return $this->formId;
    }

    /**
     * Set submission
     *
     * @param array $submission
     *
     * @return Submissions
     */
    public function setSubmission($submission)
    {
        $this->submission = $submission;

        return $this;
    }

    /**
     * Get submission
     *
     * @return array
     */
    public function getSubmission()
    {
        return $this->submission;
    }

    /**
     * Set submitDate
     *
     * @param \DateTime $submitDate
     *
     * @return Submissions
     */
    public function setSubmitDate($submitDate)
    {
        $this->submitDate = $submitDate;

        return $this;
    }

    /**
     * Get submitDate
     *
     * @return \DateTime
     */
    public function getSubmitDate()
    {
        return $this->submitDate;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
