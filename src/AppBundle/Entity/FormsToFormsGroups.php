<?php

namespace AppBundle\Entity;

/**
 * FormsToFormsGroups
 */
class FormsToFormsGroups
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $formsId;

    /**
     * @var int
     */
    private $formsGroupId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set formsId
     *
     * @param integer $formsId
     *
     * @return FormsToFormsGroups
     */
    public function setFormsId($formsId)
    {
        $this->formsId = $formsId;

        return $this;
    }

    /**
     * Get formsId
     *
     * @return int
     */
    public function getFormsId()
    {
        return $this->formsId;
    }

    /**
     * Set formsGroupId
     *
     * @param integer $formsGroupId
     *
     * @return FormsToFormsGroups
     */
    public function setFormsGroupId($formsGroupId)
    {
        $this->formsGroupId = $formsGroupId;

        return $this;
    }

    /**
     * Get formsGroupId
     *
     * @return int
     */
    public function getFormsGroupId()
    {
        return $this->formsGroupId;
    }
}
