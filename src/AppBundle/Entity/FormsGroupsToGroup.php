<?php

namespace AppBundle\Entity;

/**
 * FormsGroupsToGroup
 */
class FormsGroupsToGroup
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $formsGroupId;

    /**
     * @var int
     */
    private $groupId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set formsGroupId
     *
     * @param integer $formsGroupId
     *
     * @return FormsGroupsToGroup
     */
    public function setFormsGroupId($formsGroupId)
    {
        $this->formsGroupId = $formsGroupId;

        return $this;
    }

    /**
     * Get formsGroupId
     *
     * @return int
     */
    public function getFormsGroupId()
    {
        return $this->formsGroupId;
    }

    /**
     * Set groupId
     *
     * @param integer $groupId
     *
     * @return FormsGroupsToGroup
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId
     *
     * @return int
     */
    public function getGroupId()
    {
        return $this->groupId;
    }
}
