<?php

namespace AppBundle\Entity;

/**
 * Tokens
 */
class Tokens
{
    /**
     * @var integer
     */
    private $id;
    /**
     * @var string
     */
    private $token;


    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set token
     *
     * @param $token
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var string
     */
    private $institutionId;

    /**
     * @var string
     */
    private $genderId;

    /**
     * @var string
     */
    private $provinceId;

    /**
     * @var string
     */
    private $populationId;

    /**
     * @var string
     */
    private $fatherEducationId;

    /**
     * @var string
     */
    private $motherEducationId;


    /**
     * Set institutionId
     *
     * @param string $institutionId
     *
     * @return Tokens
     */
    public function setInstitutionId($institutionId)
    {
        $this->institutionId = $institutionId;

        return $this;
    }

    /**
     * Get institutionId
     *
     * @return string
     */
    public function getInstitutionId()
    {
        return $this->institutionId;
    }

    /**
     * Set genderId
     *
     * @param string $genderId
     *
     * @return Tokens
     */
    public function setGenderId($genderId)
    {
        $this->genderId = $genderId;

        return $this;
    }

    /**
     * Get genderId
     *
     * @return string
     */
    public function getGenderId()
    {
        return $this->genderId;
    }

    /**
     * Set provinceId
     *
     * @param string $provinceId
     *
     * @return Tokens
     */
    public function setProvinceId($provinceId)
    {
        $this->provinceId = $provinceId;

        return $this;
    }

    /**
     * Get provinceId
     *
     * @return string
     */
    public function getProvinceId()
    {
        return $this->provinceId;
    }

    /**
     * Set populationId
     *
     * @param string $populationId
     *
     * @return Tokens
     */
    public function setPopulationId($populationId)
    {
        $this->populationId = $populationId;

        return $this;
    }

    /**
     * Get populationId
     *
     * @return string
     */
    public function getPopulationId()
    {
        return $this->populationId;
    }

    /**
     * Set fatherEducationId
     *
     * @param string $fatherEducationId
     *
     * @return Tokens
     */
    public function setFatherEducationId($fatherEducationId)
    {
        $this->fatherEducationId = $fatherEducationId;

        return $this;
    }

    /**
     * Get fatherEducationId
     *
     * @return string
     */
    public function getFatherEducationId()
    {
        return $this->fatherEducationId;
    }

    /**
     * Set motherEducationId
     *
     * @param string $motherEducationId
     *
     * @return Tokens
     */
    public function setMotherEducationId($motherEducationId)
    {
        $this->motherEducationId = $motherEducationId;

        return $this;
    }

    /**
     * Get motherEducationId
     *
     * @return string
     */
    public function getMotherEducationId()
    {
        return $this->motherEducationId;
    }
}
