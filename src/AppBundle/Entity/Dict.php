<?php

namespace AppBundle\Entity;

/**
 * Dict
 */
class Dict
{
    /**
     * @var string
     */
    private $slag;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set slag
     *
     * @param string $slag
     *
     * @return Dict
     */
    public function setSlag($slag)
    {
        $this->slag = $slag;

        return $this;
    }

    /**
     * Get slag
     *
     * @return string
     */
    public function getSlag()
    {
        return $this->slag;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Dict
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
