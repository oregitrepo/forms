<?php

namespace AppBundle\Entity;

/**
 * Forms
 */
class Forms
{
    /**
     * @var array
     */
    private $form;

    /**
     * @var integer
     */
    private $reportId;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     *
     * @return Forms
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Set form
     *
     * @param array $form
     *
     * @return Forms
     */
    public function setForm($form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form
     *
     * @return array
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set reportId
     *
     * @param integer $reportId
     *
     * @return Forms
     */
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;

        return $this;
    }

    /**
     * Get reportId
     *
     * @return integer
     */
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

