<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 30.05.17
 * Time: 08:48
 */

namespace AppBundle\Constraints;


use FOS\UserBundle\Model\UserManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class EmailExistsValidator extends ConstraintValidator
{
    private $um;

    public function __construct(UserManager $um)
    {
        $this->um = $um;
    }


    public function validate($value, Constraint $constraint)
    {
        if($this->um->findUserByEmail($value))
        {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{string}}', $value)
                ->addViolation();
        }

    }
}