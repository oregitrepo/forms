<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 30.05.17
 * Time: 08:48
 */

namespace AppBundle\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EmailExists extends Constraint
{
    public $message = 'Email {{string}} already exists.';
}