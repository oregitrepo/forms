<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 30.05.17
 * Time: 08:48
 */

namespace AppBundle\Constraints;



use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class UsernameExistsValidator extends ConstraintValidator
{
    private $um;

    /**
     * UsernameExistsValidator constructor.
     * @param $um
     */
    public function __construct(UserManager $um)
    {
        $this->um = $um;
    }


    public function validate($value, Constraint $constraint)
    {
        if($this->um->findUserByUsername($value))
        {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{string}}', $value)
                ->addViolation();
        }

    }
}