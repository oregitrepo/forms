<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/forms-groups")
 */
class FormsGroupsController extends Controller
{
    /**
     * @Route("/", name="view_forms_groups", options={"expose"=true})
     */
    public function indexAction()
    {
        return $this->render('@App/formsGroups/formsGroups.html.twig');
    }

    /**
     * @Route("/group-manager/{id}", name="view_group_manager", options={"expose"=true})
     */
    public function groupManagerIndexAction($id)
    {
        $data = $this->get('app.forms_groups_query_srv')->getFormsGroupById($id);
        return $this->render('@App/formsGroups/editFormsGroups.html.twig', [
            'id' => $id,
            'data' => $data
        ]);
    }

    /**
     * @Route("/forms-manager/{id}", name="view_forms_manager", options={"expose"=true})
     */
    public function formsManagerIndexAction($id)
    {
        $data = $this->get('app.forms_groups_query_srv')->getFormsGroupById($id);
        return $this->render('@App/formsGroups/editForms.html.twig', [
            'id' => $id,
            'data' => $data
        ]);
    }

    /**
     * @Route("/get", name="get_forms_groups", options={"expose"=true})
     */
    public function getFormsGroupsAction()
    {
        $result = $this->get('app.forms_groups_query_srv')->getFormsGroupsByParentId(0);
        return new JsonResponse($result);
    }

    /**
     * @Route("/edit", name="edit_forms_groups", options={"expose"=true})
     */
    public function editFormsGroupAction(Request $request)
    {
        $name = $request->request->get('name');
        $description = $request->request->get('description');
        $id = $request->request->get('id');
        $result = $this->get('app.forms_groups_cmd_srv')->editFormsGroup($id, $name, $description);

        return new JsonResponse($result);
    }

    /**
     * @Route("/delete", name="delete_forms_groups", options={"expose"=true})
     */
    public function deleteFormsGroupAction(Request $request)
    {
        $id = $request->request->get('id');
        $result = $this->get('app.forms_groups_cmd_srv')->deleteFormsGroupById($id);
        return new JsonResponse($result);
    }


    /**
     * @Route("/delete/cascade", name="delete_forms_groups_cascade", options={"expose"=true})
     */
    public function deleteFormsGroupCascadeAction(Request $request)
    {
        $data = $request->request->get('tree');
        $result = $this->get('app.forms_groups_cmd_srv')->deleteFormsGroupCascade($data);
        return new JsonResponse($result);
    }

    /**
     * @Route("/add", name="add_forms_groups", options={"expose"=true})
     */
    public function addFormsGroupAction(Request $request)
    {
        $name = $request->request->get('name');
        $description = $request->request->get('description');
        $parentId = $request->request->get('parentId');
        $level = $request->request->get('level');
        $result = $this->get('app.forms_groups_cmd_srv')->addFormsGroup($name, $description,$parentId, $level);
        return new JsonResponse($result);
    }

    /**
     * @Route("/tree/", name="get_forms_groups_tree", options={"expose"=true})
     */
    public function getFormsGroupsTreeAction()
    {
        $result = $this->get('app.forms_groups_query_srv')->getFormsGroupsTree();
        return new JsonResponse($result);
    }
}
