<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 12.05.17
 * Time: 08:59
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FormsController extends Controller
{

    /**
     * @Route("/forms", name="view_forms" ,options={"expose"=true})
     */
    public function renderAction()
    {
        return $this->render('@App/forms/forms.html.twig');
    }

    /**
     * @Route("/forms/render", name="view_forms_render" ,options={"expose"=true})
     */
    public function indexAction()
    {
        $forms = $this->get('app.forms_query_srv')->getAllForms();
        return new JsonResponse(['forms'=>$forms]);
    }

    /**
     * @Route("/form/{id}/edit", name="edit_form" ,options={"expose"=true})
     */
    public function editFormAction($id)
    {
        $form = $this->get('app.forms_query_srv')->getFormById($id);
        return $this->render('@App/forms/editForm.html.twig',[
            'report' => $form['reportId'],
            'form'=>$form,
            'id' => $id]);
    }

    /**
     * @Route("/form/{id}/show", name="show_form", options={"expose"=true})
     */
    public function showFormAction($id)
    {
        return $this->render('@App/forms/showForm.html.twig',['id'=>$id]);
    }

    /**
     * @Route("/form/{id}/show/render", name="show_form_render", options={"expose"=true})
     */
    public function renderShowForm($id)
    {
        $form = $this->get('app.forms_query_srv')->getFormById($id);
        return new JsonResponse($form);
    }

    /**
     * @Route("/form/edit/render", name="edit_form_render" ,options={"expose"=true})
     */
    public function renderEditFormsAction(Request $request)
    {
        $formId = $request->get('formId');
        $title = $request->get('title');
        $formsGroupId = $request->get('formsGroupId');
        $reportId = $request->get('reportId');
        $this->get('app.forms_cmd_srv')->setReport($formId,$reportId);
        $this->get('app.forms_to_forms_groups_cmd_srv')->updateFormsGroupsToForm($formId, $formsGroupId);
        $form = $this->get('app.forms_cmd_srv')->setTitle($formId,$title);
        return new JsonResponse($form);
    }

    /**
     * @Route("/form-create/render", name="create_form_render")
     */
    public function renderCreateFormAction()
    {
        return $this->render('@App/forms/createForm.html.twig');
    }

    /**
     * @Route("/form-create", name="create_form", options={"expose"=true})
     */
    public function createFormAction(Request $request)
    {
        $formJson = $request->get('formJson');
        $reportId = $request->get('reportId');
        $validateJson = $this->get('app.json_validator')->checkJson($formJson,$reportId);
        return new JsonResponse($validateJson);
    }

    /**
     * @Route("/form/add/form/group", name="add_form_to_forms_group", options={"expose"=true})
     */
    public function addFormsByFormsGroupAction(Request $request)
    {
        $formsGroupId = $request->request->get('formsGroupId');
        $selectedForm = $request->request->get('selectedForm');
        dump($selectedForm);
        $result = $this->get('app.forms_to_forms_groups_cmd_srv')->addFormToGroups($selectedForm, $formsGroupId);
        return new JsonResponse($result);
    }

    /**
     * @Route("/form/status", name="status_form", options={"expose"=true})
     */
    public function statusFormAction(Request $request)
    {
        $formId = $request->get('formId');
        $status = $this->get('app.forms_cmd_srv')->setStatus($formId);
        $forms = $this->get('app.forms_query_srv')->getAllForms();
        $result = ['status' => $status,
                    'forms' => $forms ];
        return new JsonResponse($result);
    }
}