<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 06.06.17
 * Time: 14:30
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/test")
 */
class TestFormController extends Controller
{
    /**
     * @Route("/form/", name="test_form")
     */
    public function testFormAction()
    {
        return $this->render('@App/test/testForm.html.twig');
    }
}