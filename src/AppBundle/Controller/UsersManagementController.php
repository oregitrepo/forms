<?php

namespace AppBundle\Controller;

use AppBundle\Entity\FosUser;
use AppBundle\Entity\Group;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


/**
 * @Route("/users")
 */
class UsersManagementController extends Controller
{
    /**
     * @Route("/" , name="view_users" )
     */
    public function indexAction()
    {
        return $this->render('@App/users/usersManagement.html.twig');
    }

    /**
     * @Route("/update", name="update_user" , options={"expose"= "true"})
     */
    public function editUserAction(Request $request)
    {
        $userData = $request->request->all();
        $result = $this->get('app.users_cmd_srv')->updateUser($userData);
        return new JsonResponse($result);
    }

    /**
     * @Route("/get", name="get_users", options={"expose"= "true"})
     */
    public function getUsersAction()
    {
        $users = $this->get('app.users_query_srv')->getAllUsers();
        $groups = $this->get('fos_user.group_manager')->findGroups();
        $groups = array_map(function ($a){return $a->getName();}, $groups);

        return new JsonResponse([
            'users' => $users,
            'groups'=> $groups
        ]);
    }

    /**
     * @Route("/create", name="create_user", options={"expose"= "true"})
     */
    public function newUserAction(Request $request)
    {
        $userData = $request->request->all();
        $result = $this->get('app.users_cmd_srv')->createUser($userData);
        return new JsonResponse($result);
    }

    /**
     * @Route("/delete", name="delete_user", options={"expose"= "true"})
     */
    public function deleteUserAction(Request $request)
    {
        $userId = $request->request->get('id');
        $result = $this->get('app.users_cmd_srv')->deleteUser($userId);
        return new JsonResponse($result);
    }
}
