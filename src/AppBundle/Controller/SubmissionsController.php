<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/submissions")
 */
class SubmissionsController extends Controller
{
    /**
     * @Route("/", name="view_submissions")
     */
    public function indexAction()
    {
        return $this->render('@App/submissions/submissions.html.twig');
    }
    /**
     * @Route("/get/", name="get_submissions", options= {"expose" = true});
     */
    public function getSubmissionsAction(Request $request)
    {
        $token = $request->get('token');
        $userId = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $result = $this->get('app.submissions_query_srv')->getFormsWithSubmissionsByTokenAndUserId($token,$userId);
        return new JsonResponse($result);
    }

    /**
     * @Route("/{submissionId}/edit/render", name="edit_submission_view" ,options={"expose"=true})
     */
    public function editSubmissionViewAction($submissionId)
    {
        return $this->render('@App/submissions/editSubmission.html.twig',[
            'submissionId'=>$submissionId
        ]);
    }

    /**
     * @Route("/{submissionId}/edit/submission", name="get_submission_form", options={"expose"=true})
     */
    public function getSubmissionFormAction($submissionId)
    {
        $submission = $this->get('app.submissions_query_srv')->getSubmissionById($submissionId);
        $formId = $submission['formId'];
        $form = $this->get('app.forms_query_srv')->getFormById($formId);

        return new JsonResponse([
            'form' => $form,
            'submission' => $submission
        ]);
    }

    /**
     * @Route("/update", name="update_submission", options={"expose"=true})
     */
    public function updateSubmissionAction(Request $request)
    {
        $updateData = $request->get('data');
        $submissionId = $request->get('id');
        $submissionFormId = $request->get('formId');

        $result = $this->get('app.submissions_cmd_srv')->updateSubmission($submissionFormId,$submissionId,$updateData);
        return new JsonResponse($result);
    }

    /**
     * @Route("/delete", name="delete_submission", options={"expose"=true})
     */
    public function deleteSubmissionAction(Request $request)
    {
        $formId = $request->get('formId');
        $token = $request->get('token');
        $userId = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $delete = $this->get('app.submissions_cmd_srv')->deleteSubmissionById($formId);
        $submissions = $this->get('app.submissions_query_srv')->getFormsWithSubmissionsByTokenAndUserId($token,$userId);

        return new JsonResponse([
            'delete' => $delete,
            'submissions' => $submissions
        ]);
    }
}
