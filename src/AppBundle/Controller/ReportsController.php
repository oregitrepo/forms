<?php

namespace AppBundle\Controller;
    
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/reports")
 */
class ReportsController extends Controller
{

    /**
     * @Route("/", name="reports", options={"expose"=true})
     */
    public function indexAction()
    {
        return $this->render('@App/reports/reports.html.twig');
    }

    /**
     * @Route("/get/all", name="reports_get_all", options={"expose"=true})
     */
    public function getAllReportsAction(Request $request)
    {
        $token = $request->request->get('token');
        $reports = $this->get('app.reports_data_query_srv')->getAllReports($token);
        return new JsonResponse($reports);
    }

    /**
     * @Route("/{id}/get", name="report_get", options={"expose"=true})
     */
    public function getReportAction($id)
    {
        $result = $this->get('app.reports_data_query_srv')->getReportResult($id);
        return $this->render('@App/reports/result.html.twig', [
            'result' => $result
        ]);
    }

    /**
     * @Route("/get/data", name="reports_get", options={"expose"=true})
     */
    public function getReportsAction()
    {
        $reports = $this->get('app.reports_data_query_srv')->getReports();
        return new JsonResponse($reports);
    }


}
