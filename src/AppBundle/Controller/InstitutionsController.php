<?php
/**
 * Project: forms
 * File: InstitutionsController.php
 * Creator: Konrad "Tree" Słotwiński
 * Date: 06.06.17
 * Time: 13:23
 * License: MIT
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/institutions")
 */
class InstitutionsController extends Controller
{

    /**
     * @Route("/", name="institutions_index", options={"expose"=true})
     */

    public function indexAction()
    {
        $institutions = $this->get('app.institutions_query_srv')->getAllInstitutions();
        return $this->render('@App/institutions/index.html.twig', [
                'institutions' => $institutions
            ]
        );
    }

    /**
     * @Route("/create", name="institutions_create", options={"expose"=true})
     */

    public function indexCreateInstitutionAction()
    {
        $users = $this->get('app.users_query_srv')->getAllUsers();
        return $this->render('@App/institutions/create.html.twig', [
                'users' => $users
            ]
        );
    }


    /**
     * @Route("/{id}/edit", name="institutions_edit", options={"expose"=true})
     */

    public function indexEditInstitutionAction($id)
    {
        $institution = $this->get('app.institutions_query_srv')->getInstitutionById($id);
        $users = $this->get('app.users_query_srv')->getAllUsers();
        return $this->render('@App/institutions/edit.html.twig', [
                'users' => $users,
                'institution' => $institution
            ]
        );

    }

    /**
     * @Route("/add", name="institutions_add", options={"expose"=true})
     */

    public function addInstitutionAction(Request $request)
    {
        $name = $request->request->get('name');
        $description = $request->request->get('description');
        $userId = $request->request->get('redactor');
        $result = $this->get('app.institutions_cmd_srv')->addInstitution($name, $description, $userId);
        return new JsonResponse($result);
    }

    /**
     * @Route("/delete", name="institutions_delete", options={"expose"=true})
     */

    public function deleteInstitutionAction(Request $request)
    {
        $id = $request->request->get('id');
        $result = $this->get('app.institutions_cmd_srv')->deleteInstitutionById($id);
        return new JsonResponse($result);
    }

    /**
     * @Route("/update", name="institutions_update", options={"expose"=true})
     */

    public function updateInstitutionAction(Request $request)
    {
        $id = $request->request->get('id');
        $name = $request->request->get('name');
        $description = $request->request->get('description');
        $userId = $request->request->get('redactor');
        $result = $this->get('app.institutions_cmd_srv')->updateInstitution($id, $name, $description, $userId);
        return new JsonResponse($result);
    }
}