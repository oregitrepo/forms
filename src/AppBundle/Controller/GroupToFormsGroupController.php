<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/forms-groups/group", name="groups_to_forms_group")
 */
class GroupToFormsGroupController extends Controller
{
    /**
     * @Route("/get", name="get_groups_to_forms_group", options={"expose"=true})
     */
    public function getGroupsByFormsGroupAction(Request $request)
    {
        $id = $request->request->get('id');
        $result = $this->get('app.forms_groups_to_group_query_srv')->getGroupsByFormsGroupId($id);
        $groups = $this->get('app.groups_query_srv')->getAllGroups();
        return new JsonResponse([
            'result' => $result,
            'groups' => $groups
        ]);
    }

    /**
     * @Route("/delete", name="delete_groups_to_forms_group", options={"expose"=true})
     */
    public function deleteGroupByFormsGroupAction(Request $request)
    {
        $formsGroupId = $request->request->get('formsGroupId');
        $selectedGroup = $request->request->get('selectedGroup');
        $result = $this->get('app.forms_groups_to_group_cmd_srv')->deleteGroup($selectedGroup, $formsGroupId);
        return new JsonResponse($result);
    }

    /**
     * @Route("/add", name="add_groups_to_forms_group", options={"expose"=true})
     */
    public function addGroupByFormsGroupAction(Request $request)
    {
        $formsGroupId = $request->request->get('formsGroupId');
        $selectedGroup = $request->request->get('selectedGroup');
        $result = $this->get('app.forms_groups_to_group_cmd_srv')->addGroup($selectedGroup, $formsGroupId);
        return new JsonResponse($result);
    }
}
