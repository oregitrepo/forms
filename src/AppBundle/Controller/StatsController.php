<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/stats")
 */
class StatsController extends Controller
{
    /**
     * @Route("/", name="stats", options={"expose"=true})
     */
    public function indexAction()
    {
        $forms = $this->get('app.stats_query_srv')->getFormsWithSubmissions();
        return $this->render('@App/stats/stats.html.twig', [
            'forms' => $forms
        ]);
    }

    /**
     * @Route("/{id}/get", name="stats_get", options={"expose"=true})
     */
    public function getStatsAction($id)
    {
        $result = $this->get('app.stats_query_srv')->getFormStats($id);
        $title = $this->get('app.forms_query_srv')->getFormById($id)['form']['title'];
        return $this->render('@App/stats/formStats.html.twig', [
            'result' => $result,
            'title' => $title
        ]);
    }
}
