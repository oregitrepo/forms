<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/token")
 */
class TokensController extends Controller
{

    /**
     * @Route("/generator", name="token_generator", options={"expose"=true})
     */
    public function generatorIndexAction()
    {
        $dicts = $this->get('app.dict_query_srv')->getAllDictsWithValues();
        return $this->render('@App/tokens/generator.html.twig', [
            'gender' => $dicts['gender'],
            'province' => $dicts['province'],
            'population' => $dicts['population'],
            'education' => $dicts['education'],
        ]);
    }

    /**
     * @Route("/manager", name="token_manager", options={"expose"=true})
     */
    public function managerIndexAction()
    {
        $tokens = $this->get('app.tokens_query_srv')->getAllTokens();
        $institutions = $this->get('app.institutions_query_srv')->getAllInstitutions();
        return $this->render('@App/tokens/manager.html.twig', [
            'tokens' => $tokens,
            'institutions' => $institutions
        ]);
    }

    /**
     * @Route("/assign", name="token_assign", options={"expose"=true})
     */
    public function assignTokenToInstitutionAction(Request $request)
    {
        $tokenId = $request->request->get('tokenId');
        $institutionId = $request->request->get('institutionId');
        $result = $this->get('app.tokens_cmd_srv')->assignTokenToInstitution($tokenId, $institutionId);
        return new Response($result);
    }

    /**
     * @Route("/generate", name="token_generate", options={"expose"=true})
     */
    public function generateTokenAction(Request $request)
    {
        $name = $request->request->get('name');
        $surname = $request->request->get('surname');
        $pesel = $request->request->get('pesel');
        $token = $this->get('app.token_generator_srv')->getToken($name, $surname, $pesel);
        return new Response($token);
    }

    /**
     * @Route("/add", name="token_add", options={"expose"=true})
     */
    public function addTokenAction(Request $request)
    {
        $token = $request->get('token');
        $genderId = $request->get('gender');
        $provinceId = $request->get('province');
        $populationId = $request->get('population');
        $fatherEducationId = $request->get('father_education');
        $motherEducationId = $request->get('mother_education');
        $result = $this->get('app.tokens_cmd_srv')->addNewToken($token, $genderId, $provinceId, $populationId, $fatherEducationId, $motherEducationId);
        return new Response($result);
    }

    /**
     * @Route("/cookie/set", name="token_cookie_set", options={"expose"=true})
     */
    public function setTokenCookieAction(Request $request)
    {
        $userId = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $token = $request->get('token');
        $result = $this->get('app.tokens_query_srv')->doesTokenExists($token, $userId);
        return new JsonResponse($result);
    }
}
