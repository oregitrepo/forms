<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/cards")
 */
class CardsController extends Controller
{

    /**
     * @Route("/", name="cards_index", options={"expose"=true})
     */
    public function indexAction()
    {
        $userId = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $tokens = $this->get('app.tokens_query_srv')->getTokensByUserId($userId);
        return $this->render('@App/cards/index.html.twig', [
            'tokens' => $tokens
        ]);
    }

    /**
     * @Route("/{id}/transfer", name="cards_transfer", options={"expose"=true})
     */
    public function indexTransferAction($id)
    {
        $token = $this->get('app.tokens_query_srv')->getTokenById($id);
        $institutions = $this->get('app.institutions_query_srv')->getAllInstitutions();
        return $this->render('@App/cards/transfer.html.twig', [
            'token' => $token,
            'institutions' => $institutions
        ]);
    }

    /**
     * @Route("/{id}/assign", name="cards_assign", options={"expose"=true})
     */
    public function indexAssignUsersAction($id)
    {
        $token = $this->get('app.tokens_query_srv')->getTokenById($id);
        $groups = $this->get('app.groups_query_srv')->getAllGroups();
        return $this->render('@App/cards/assign.html.twig', [
            'groups' => $groups,
            'token' => $token
        ]);
    }

    /**
     * @Route("/transfer/set", name="cards_transfer_set", options={"expose"=true})
     */
    public function setTokenInstitutionAction(Request $request)
    {
        $tokenId = $request->request->get('token');
        $institutionId = $request->request->get('institution');
        $result = $this->get('app.tokens_cmd_srv')->assignTokenToInstitution($tokenId, $institutionId);
        return new JsonResponse($result);
    }

    /**
     * @Route("/users/get", name="cards_users_connections_get", options={"expose"=true})
     */
    public function getUsersAndConnectionsAction(Request $request)
    {
        $id = $request->request->get('id');
        $tokenId = $request->request->get('token');
        $connections = $this->get('app.tokens_users_query_srv')->getConnectionsByTokenId($tokenId);
        $users = $this->get('app.users_query_srv')->getUsersByGroupId($id);
        $result = ['users' => $users, 'connections' => $connections];
        return new JsonResponse($result);
    }

    /**
     * @Route("/connection/add", name="cards_connection_add", options={"expose"=true})
     */
    public function addTokenUserAction(Request $request)
    {
        $userId = $request->request->get('user');
        $tokenId = $request->request->get('token');
        $token = $this->get('app.tokens_query_srv')->getTokenById($tokenId);
        $result = $this->get('app.tokens_users_cmd_srv')->addConnection($userId, $tokenId, $token['institutionId']);
        return new JsonResponse($result);
    }

    /**
     * @Route("/connection/delete", name="cards_connection_delete", options={"expose"=true})
     */
    public function deleteTokenUserAction(Request $request)
    {
        $userId = $request->request->get('user');
        $tokenId = $request->request->get('token');
        $token = $this->get('app.tokens_query_srv')->getTokenById($tokenId);
        $result = $this->get('app.tokens_users_cmd_srv')->deleteConnection($userId, $tokenId, $token['institutionId']);
        return new JsonResponse($result);
    }
}
