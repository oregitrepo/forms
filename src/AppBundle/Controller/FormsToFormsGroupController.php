<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/forms-groups/forms", name="forms_to_forms_group")
 */
class FormsToFormsGroupController extends Controller
{

    /**
     * @Route("/get", name="get_forms_to_forms_group", options={"expose"=true})
     */
    public function getFormsByFormsGroupAction(Request $request)
    {
        $id = $request->request->get('id');
        $result = $this->get('app.forms_to_forms_groups_query_srv')->getFormsByFormsGroupId($id);
        $forms = $this->get('app.forms_query_srv')->getAllActiveForms();
        return new JsonResponse([
            'result' => $result,
            'forms' => $forms
        ]);
    }

    /**
     * @Route("/delete", name="delete_forms_to_forms_group", options={"expose"=true})
     */
    public function deleteFormByFormsGroupAction(Request $request)
    {
        $formsGroupId = $request->request->get('formsGroupId');
        $selectedForm = $request->request->get('selectedForm');
        $result = $this->get('app.forms_to_forms_groups_cmd_srv')->deleteForm($selectedForm, $formsGroupId);
        return new JsonResponse($result);
    }

    /**
     * @Route("/add", name="add_forms_to_forms_group", options={"expose"=true})
     */
    public function addFormByFormsGroupAction(Request $request)
    {
        $formsGroupId = $request->request->get('formsGroupId');
        $selectedForm = $request->request->get('selectedForm');
        $result = $this->get('app.forms_to_forms_groups_cmd_srv')->addForm($selectedForm, $formsGroupId);
        return new JsonResponse($result);
    }

    /**
     * @Route("/get/groups" ,name="get_forms_groups_form", options={"expose"=true})
     */
    public function getFormsGroupsByFormId(Request $request)
    {
        $formId = $request->get('formId');
        $groupsId = $this->get('app.forms_to_forms_groups_cmd_srv')->getFormsGroupsByFormId($formId);

        return new JsonResponse($groupsId);
    }
}
