<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SubmitFormController extends Controller
{

    /**
     * @Route("/form/view/{formId}", name="form", options={"expose"=true})
     */
    public function indexAction($formId)
    {
        return $this->render('@App/forms/form.html.twig',['formId' => $formId]);
    }

    /**
     * @Route("/form/render", name="render_form", options={"expose"=true})
     */
    public function renderFormAction(Request $request)
    {
        $formId = $request->get('formId');
        $form = $this->get('app.forms_query_srv')->getFormById($formId);
        return new JsonResponse($form);
    }

    /**
     * @Route("/form/submit", name="submit_form", options={"expose"=true})
     */
    public function submitFormAction(Request $request)
    {
        $formData = $request->get('data');
        $formId = $request->get('id');
        $token = $request->get('token');

        $userId = $this->get('security.token_storage')->getToken()->getUser()->getId();

        if(($this->get('app.tokens_query_srv')->doesTokenExists($token, $userId))){
            $result = $this->get('app.submissions_cmd_srv')->submitForm($token,$userId,$formId, $formData);
            $result = ['result'=>$result];
        }
        else{
            $result = ['result'=>'Podany token nie istnieje'];
        }

        return new JsonResponse($result);
    }

}
