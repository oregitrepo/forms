<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/user-forms")
 */
class UserFormsController extends Controller
{
    /**
     * @Route("/", name="view_user_forms")
     */
    public function indexAction()
    {
        return $this->render('@App/forms/userForms.html.twig');
    }

    /**
     * @Route("/get", name="get_user_forms", options={"expose"=true})
     */
    public function getUserFormsAction()
    {
        $userId = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $result = $this->get('app.user_forms_query_srv')->getFormsGroupsByUserId($userId);
        return new JsonResponse($result);
    }
}