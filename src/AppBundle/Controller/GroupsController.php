<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/groups")
 */
class GroupsController extends Controller
{

    /**
     * @Route("/", name="groups", options={"expose"=true})
     */
    public function indexGroupsAction()
    {
        return $this->render('@App/groups/groups.html.twig');
    }

    /**
     * @Route("/add", name="group_add", options={"expose"=true})
     */
    public function indexCreateGroupAction()
    {
        return $this->render('@App/groups/addGroup.html.twig');
    }

    /**
     * @Route("/{id}/edit", name="group_edit", options={"expose"=true})
     */
    public function indexEditGroupAction($id)
    {
        $group = $this->get('app.groups_query_srv')->getGroupById($id);
        return $this->render('@App/groups/editGroup.html.twig', [
            'group' => $group
        ]);
    }

    /**
     * @Route("/get/all", name="groups_get_all", options={"expose"=true})
     */
    public function getAllGroupsAction()
    {
        $groups = $this->get('app.groups_query_srv')->getAllGroups();
        return new JsonResponse($groups);
    }

    /**
     * @Route("/roles/get/all", name="groups_roles_get_all", options={"expose"=true})
     */
    public function getAllPermissionsAction()
    {
        $roles = $this->getParameter('security.role_hierarchy.roles');
        return new JsonResponse($roles);
    }

    /**
     * @Route("/create", name="group_create", options={"expose"=true})
     */
    public function createGroupAction(Request $request)
    {
        $name = $request->request->get('name');
        $roles = $request->request->all();
        unset($roles['name']);
        $result = $this->get('app.groups_cmd_srv')->createGroup($name, $roles);
        return new Response($result);
    }

    /**
     * @Route("/update", name="group_update", options={"expose"=true})
     */
    public function updateGroupAction(Request $request)
    {
        $id = $request->request->get('id');
        $name = $request->request->get('name');
        $roles = $request->request->all();
        unset($roles['name']);
        unset($roles['id']);
        $result = $this->get('app.groups_cmd_srv')->updateGroup($id, $name, $roles);
        return new Response($result);
    }

    /**
     * @Route("/{id}/delete", name="group_delete", options={"expose"=true})
     */
    public function deleteGroupAction($id)
    {
        $result = $this->get('app.groups_cmd_srv')->deleteGroup($id);
        return new Response($result);
    }
}
