<?php

namespace AppBundle\Reports;

abstract class Report
{
    private $result;

    abstract public function __construct(ReportDataContainer $data);

    public function getResult()
    {
        return $this->result;
    }

    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }
}