<?php

namespace AppBundle\Reports;

class Talents extends Report
{
    private $submission;
    private $report;

    public function __construct(ReportDataContainer $data)
    {
        $this->submission = $data->getSubmissionData()->getSubmission();
        $this->report = $data->getReportData()->getData();
        $this->setResult($this->resultByPointsSum());
    }

    public function pointsPercentage($topic)
    {
        $sum = 0;
        foreach ($this->submission[$topic] as $field) {
            $sum += (int)$field;
        }
        $max = 2 * (count($this->submission[$topic]));
        $percentage = ($sum / $max) * 100;
        return $percentage;
    }

    public function resultByPointsSum()
    {
        $result = '';
        foreach ($this->report as $topic => $params) {
            foreach ($params as $param) {
                $percentage = $this->pointsPercentage($topic);
                if ($param['range']['minPerc'] <= $percentage && $param['range']['maxPerc'] >= $percentage) {
                    $result = $result . "<p>" . $param['result'] . "</p><br>";
                }
            }
        }

        return $result ?? 'Nieznany wynik';
    }
}