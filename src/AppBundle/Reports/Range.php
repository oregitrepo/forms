<?php

namespace AppBundle\Reports;

class Range extends Report
{
    private $submission;
    private $report;

    public function __construct(ReportDataContainer $data)
    {
        $this->submission = $data->getSubmissionData()->getSubmission();
        $this->report = $data->getReportData()->getData();
        $this->setResult($this->calculateResult());
    }

    public function calculateResult()
    {
        return $this->resultByPointsSum($this->pointsSum());
    }

    public function pointsSum()
    {
        $sum = 0;
        foreach ($this->submission as $field) {
            $sum += (int)$field;
        }
        return $sum;
    }

    public function resultByPointsSum($sum)
    {
        foreach ($this->report as $label) {
            if ($label['range']['minPoints'] <= $sum && $label['range']['maxPoints'] >= $sum) {
                $result = $label['result'];
                break;
            }
        }
        return $result ?? 'Nieznany wynik';
    }
}