<?php

namespace AppBundle\Reports;

use AppBundle\Entity\Forms;
use AppBundle\Entity\Reports;
use AppBundle\Entity\Submissions;

class ReportDataContainer
{
    private $submissionData;
    private $reportData;
    private $formData;
    private $class;

    public function __construct(Reports $reportData, Submissions $submissionData, Forms $formData)
    {
        $this->reportData = $reportData;
        $this->submissionData = $submissionData;
        $this->formData = $formData;
        $this->class = $reportData->getClass();
    }

    /**
     * @return Submissions
     */
    public function getSubmissionData()
    {
        return $this->submissionData;
    }

    /**
     * @param Submissions $submissionData
     * @return $this
     */
    public function setSubmissionData($submissionData)
    {
        $this->submissionData = $submissionData;
        return $this;
    }

    /**
     * @return Reports
     */
    public function getReportData()
    {
        return $this->reportData;
    }

    /**
     * @param Reports $reportData
     * @return $this
     */
    public function setReportData($reportData)
    {
        $this->reportData = $reportData;
        return $this;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return $this
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @return Forms
     */
    public function getFormData(): Forms
    {
        return $this->formData;
    }

    /**
     * @param Forms $formData
     * @return $this
     */
    public function setFormData(Forms $formData)
    {
        $this->formData = $formData;
        return $this;
    }


}